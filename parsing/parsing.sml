(* Those types are required to represent sentence trees *)

datatype NOUN = NN | NNS | NNP | NNPS | PRP | ADD
and VERB = VB | VBD | VBG | VBN | VBP | VBZ
and ADJ = JJ | JJR | JJS | CD
and Q = WRB | WP
and POSS = PRP_POSS | WP_POSS
and ADV = RB | RBR | RBS
and EX = EX
and MOD = MD
and MISC = P_CC | PDT | DT | IN | WDT | TO | P_POS | RP

and POS = p_n of NOUN
    | p_v of VERB
    | p_adj of ADJ
    | p_q of Q
    | p_poss of POSS
    | p_adv of ADV
    | p_ex of EX
    | p_mod of MOD
    | p_misc of MISC
    
and DEP = ACL | ACL_RELCL | ADVMOD | APPOS | CASE | CCOMP | CONJ | COP | DEP | EXPL | FLAT | LIST | NSUBJ | NSUBJ_PASS | OBJ | ROOT | ADVCL | AMOD | AUX | AUX_PASS | CC | COMPOUND | COMPOUND_PRT | DET | DET_PREDET | FIXED | IOBJ | MARK | NMOD | NMOD_POSS | NMOD_TMOD | NUMMOD | OBL | OBL_TMOD | OBL_NPMOD | VOCATIVE | XCOMP

and sentence = sen of string*POS*DEP*(sentence list)




(* Those are some exceptions that can be thrown if necessary *)

(* thrown by parse functions when trying to parse a wrong phrase type *)
exception WRONG_PHRASE_TYPE;

(* thrown by posFromStr if the given string cannot be translated to a valid POS *)
exception ILLEGAL_POS;

(* thrown by depFromStr if the given string cannot be translated to a valid DEP *)
exception ILLEGAL_DEP;

(* thrown if some invalid state occurs, e.g., if findS returns something else than NP or NP_VAR *)
exception ILLEGAL_STATE;

(* thrown if no suitable parsing method for a sentence's root can be found *)
exception UNPARSABLE_SENTENCE;


(* These types are required to represent formulas *)

datatype term = var of string | ID of string | truth | falsity

and formula =
        pred of term*term list
    |   impl of formula*formula
    |   conj of formula*formula
    |   disj of formula*formula
    |   exists of term*formula
    |   forall of term*formula
    |   neg of formula
    |   atomic of term




(* This function generates a fresh variable by simply counting up *)

val varCounter:int ref = ref 0;

fun freshVar():term = (varCounter := !varCounter + 1; var("?x" ^ Int.toString(!varCounter)));




(* A phrase generally consists of a higher order function mapping to a formula as well as the sentence properties POS, DEP and children *)

datatype phrase =
        AP of (((term->formula)->formula)->((term->formula)->formula))*string*POS*DEP*phrase list       (* Adjective phrase *)
    |   ADVP of (term->formula)*string*POS*DEP*phrase list                                              (* Adverb phrase *)
    |   NEGP of ((term->term->(term->formula)->formula)->term->term->(term->formula)->formula)*string*POS*DEP*phrase list                                                                                                                   (* Negation phrase *)
    |   NP of ((term->formula)->formula)*string*POS*DEP*phrase list                                     (* Noun phrase *)
    |   NP_VAR of ((term->formula)->formula)*POS*DEP*phrase list                                        (* Variable noun (placeholder) *)
    |   DETP of (((term->formula)->formula)->((term->formula)->formula))*string*POS*DEP*phrase list     (* Determiner phrase *)
    |   TV of (term->term->(term->formula)->formula)*string*POS*DEP*phrase list                         (* Transitive verb *)
    |   EXP of ((term->term->(term->formula)->formula)->(term->term->(term->formula)->formula))*string*POS*DEP*phrase list                                                                                                                   (* Existential phrase *)
    |   ADPP of (((term->formula)->formula)->(term->formula)->term->formula)*string*POS*DEP*phrase list (* Adpositional phrase *)
    |   QP of ((term->term->(term->formula)->formula)->(term->term->(term->formula)->formula))*string*POS*DEP*phrase list                                                                                                                   (* Question phrase *)
    |   POSSP of (((term->formula)->formula)->((term->formula)->formula))*string*POS*DEP*phrase list    (* Possessive phrase *)
    |   NMODQ of (((term->formula)->formula)->((term->formula)->formula))*string*POS*DEP*phrase list    (* Noun modifying question phrase *)
    |   VMOD of ((term->term->(term->formula)->formula)->(term->term->(term->formula)->formula))*string*POS*DEP*phrase list                                                                                                                 (* Verbal modifier phrase *)
    |   MODP of ((term->term->(term->formula)->formula)->(term->term->(term->formula)->formula))*string*POS*DEP*phrase list                                                                                                                 (* Modal phrases *)
    |   MISCP of string*POS*DEP*phrase list                                                             (* Miscellanious phrases *)




(* These functions print formulas as strings *)

fun tToStr(t:term):string =
    case t of
        ID s => s
    |   var v => v
    |   truth => "true"
    |   falsity => "false"

fun fToStr(f:formula):string =
    case f of
        pred (p, ts) => tToStr(p) ^ "(" ^ (foldl (fn (a, b) => if b <> "" then a ^ "," ^ b else a) "")(map (fn t => tToStr(t)) ts) ^ ")"
    |   impl (f1, f2) => "((" ^ fToStr(f1) ^ ") -> (" ^ fToStr(f2) ^ "))"
    |   conj (f1, f2) => "(" ^ fToStr(f1) ^ " & " ^ fToStr(f2) ^ ")"
    |   disj (f1, f2) => "(" ^ fToStr(f1) ^ " | " ^ fToStr(f2) ^ ")"
    |   exists (i, f) => "exists " ^ tToStr(i) ^ "." ^ fToStr(f)
    |   forall (i, f) => "forall " ^ tToStr(i) ^ "." ^ fToStr(f)
    |   neg (f) => "~(" ^ fToStr(f) ^ ")"
    |   atomic (t) => tToStr(t)




(* These functions find various phrases in a given phrase's childs *)

fun isPassive(s:sentence):bool = 
    case s of
        sen(tx, p, d, sen(_, _, NSUBJ_PASS, _) :: cs) => true
    |   sen(tx, p, d, sen(_, _, AUX_PASS, _) :: cs) => true
    |   sen(tx, p, d, _ :: cs) => isPassive(sen(tx, p, d, cs))
    |   sen(tx, p, d, nil) => false

fun findS(ps:phrase list):phrase =
    case ps of
        NP(f, tx, p_n(p), NSUBJ, cs) :: _ => NP(f, tx, p_n(p), NSUBJ, cs)
    |   NP(f, tx, p_q(WP), NSUBJ, cs) :: _ => NP(f, tx, p_q(WP), NSUBJ, cs)
    |   NP(f, tx, p_n(p), NSUBJ_PASS, cs) :: _ => NP(f, tx, p_n(p), NSUBJ_PASS, cs)
    |   NP(f, tx, p_q(WP), NSUBJ_PASS, cs) :: _ => NP(f, tx, p_q(WP), NSUBJ_PASS, cs)
    |   _ :: tail => findS(tail)
    |   nil => NP_VAR(fn x => x (freshVar()), p_n(NN), NSUBJ, [])

fun findO(ps:phrase list):phrase =
    case ps of
        NP(f, tx, p, OBJ, cs) :: _ => NP(f, tx, p, OBJ, cs)
    |   _ :: tail => findO(tail)
    |   nil => NP_VAR(fn x => x (freshVar()), p_n(NN), OBJ, [])
    
fun findCASE(ps:phrase list):phrase option =
    case ps of
        ADPP(f, tx, p, CASE, cs) :: _ => SOME(ADPP(f, tx, p, CASE, cs))
    |   _ :: tail => findCASE(tail)
    |   nil => NONE

fun findP(ps:phrase list):phrase option =
    case ps of
        TV(f, tx, p, d, cs) :: _ => SOME(TV(f, tx, p, d, cs))
    |   _ :: tail => findP(tail)
    |   nil => NONE
    
fun findCC(ps:phrase list):phrase option =
    case ps of
        MISCP(tx, p_misc(P_CC), CC, cs) :: _ => SOME(MISCP(tx, p_misc(P_CC), CC, cs))
    |   _ :: tail => findCC(tail)
    |   nil => NONE
    
fun findXCOMP(ps:phrase list):phrase option =
    case ps of
        TV(f, tx, p, XCOMP, cs) :: _ => SOME(TV(f, tx, p, XCOMP, cs))
    |   _ :: tail => findXCOMP(tail)
    |   nil => NONE

fun findEXP(ps:phrase list):phrase option =
    case ps of
        EXP(f, tx, p, d, cs) :: _ => SOME(EXP(f, tx, p, d, cs))
    |   _ :: tail => findEXP(tail)
    |   nil => NONE




(* This function converts a given sentence to a phrase which is required for triple extraction *)

fun senToPhrase(s:sentence):phrase option =
    case s of
        sen(("someone"|"somebody"|"anybody"|"anyone"|"something"|"anything"), p_n(n), d, cs) => let val x = freshVar() in (
                SOME(NP(fn P => exists(x, P x), "something", p_n(n), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen(("who"|"whom"|"what"), p_q(WP), d, cs) => let val x = freshVar() in (
                SOME(NP(fn P => exists(x, P x), "who", p_q(WP), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen("when", p_q(WRB), d, cs) => let val z = freshVar() in (
                SOME(QP(fn P => (fn x => (fn y => (fn Q => (exists(z, P x y (fn e => conj(pred(ID("{ev_TMP}"), [z, e]), Q e))))))), "when", p_q(WP), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen("where", p_q(WRB), d, cs) => let val z = freshVar() in (
                SOME(QP(fn P => (fn x => (fn y => (fn Q => (exists(z, P x y (fn e => conj(pred(ID("{ev_ARGM}"), [z, ID("where"), e]), Q e))))))), "where", p_q(WP), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen(tx, p, OBL_TMOD, cs) => SOME(VMOD(fn P => (fn x => (fn y => (fn Q => (P x y (fn e => (conj(pred(ID("{ev_TMP}"), [ID(tx), e]), Q e))))))), tx, p, OBL_TMOD, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
    |   sen(tx, p, OBL, cs) => SOME(NP(fn P => (P (ID tx)), tx, p, OBL, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
    |   sen(tx, p_n(n), d, cs) => SOME(NP(fn P => P (ID(tx)), tx, p_n(n), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
    |   sen(tx, p_v(v), d, cs) => let val z = freshVar() in (
                (case isPassive(s) of
                    true => SOME(TV(fn x => (fn y => (fn P => exists(z, conj(pred(ID("{ev_PAG}"), [x, z]), conj(pred(ID("{ev_REL}"), [ID(tx), z]), conj(pred(ID("{ev_PPT}"), [y, z]), P z)))))), tx, p_v(v), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
                |   false => SOME(TV(fn x => (fn y => (fn P => exists(z, conj(pred(ID("{ev_PAG}"), [y, z]), conj(pred(ID("{ev_REL}"), [ID(tx), z]), conj(pred(ID("{ev_PPT}"), [x, z]), P z)))))), tx, p_v(v), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
                )
            ) end
    |   sen(tx, p_adj(CD), NUMMOD, cs) => let val y = freshVar() in (
                SOME(AP(fn N => (fn P => N (fn x => exists(y, conj(pred(x, [y]), conj(P y, pred(ID("{count}"), [ID(tx), y])))))), tx, p_adj(CD), NUMMOD, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen("not", p_adv(a), d, cs) => SOME(NEGP(fn V => (fn x => (fn y => (fn P => (neg(V x y P))))), "not", p_adv(a), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
    |   sen(tx, p, CASE, cs) => SOME(ADPP(fn i => (fn P => (fn e => (i (fn x => conj(pred(ID("{ev_ARGM}"), [x, ID(tx), e]), P e))))), tx, p, CASE, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
    |   sen(tx, p_adv(a), d, cs) => SOME(ADVP(fn x => pred(ID(tx), [x]), tx, p_adv(a), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
    |   sen(tx, p_poss(PRP_POSS), d, cs) => let val z = freshVar() in (
                let val e = freshVar() in (
                    SOME(POSSP(fn x => (fn P => x (fn y => exists(z, conj((fn v => v z) P, conj(pred(y, [z]), exists(e, conj(pred(ID("{event}"), [e]), conj(pred(ID("{ev_PAG}"), [ID(tx), e]), conj(pred(ID("{ev_REL}"), [ID("{possess}"), e]), pred(ID("{ev_PPT}"), [z, e])))))))))), tx, p_poss(PRP_POSS), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
                ) end
            ) end
    |   sen(tx, p_mod(MD), d, cs) => SOME(MODP(fn P => (fn x => (fn y => (fn Q => (P x y (fn e => conj(pred(ID("{ev_MOD}"), [ID(tx), e]), Q e)))))), tx, p_mod(MD), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
    |   sen(tx, p_poss(WP_POSS), d, cs) => let val z = freshVar() in (
                let val e = freshVar() in (
                    let val v = freshVar() in (
                        SOME(NMODQ(fn x => (fn P => x (fn y => exists(v, conj((fn j => j v) P, conj(pred(y, [v]), exists(z, exists(e, conj(pred(ID("{event}"), [e]), conj(pred(ID("{ev_PAG}"), [z, e]), conj(pred(ID("{ev_REL}"), [ID("{possess}"), e]), pred(ID("{ev_PPT}"), [v, e]))))))))))), tx, p_poss(WP_POSS), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
                    ) end
                ) end
            ) end
    |   sen(tx, p_misc(WDT), d, cs) => let val z = freshVar() in (
                SOME(NMODQ(fn x => (fn P => (exists(z, conj((P z), (x (fn y => (pred(y, [z])))))))), tx, p_misc(WDT), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen(("how many" | "how much"), p_q(WRB), d, cs) => let val z = freshVar() in (
                    let val v = freshVar() in (
                        SOME(NMODQ(fn N => (fn P => exists(z, N (fn y => exists(v,
                conj(conj(P v, pred(y, [v])), pred(ID("{count}"), [z, v])))))), "how many", p_q(WRB), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
                    ) end
            ) end
    |   sen(tx, p_ex(EX), d, cs) => let val z = freshVar() in (
                SOME(EXP(fn v => (fn x => (fn y => (fn P => (exists(z, pred(y, [z])))))), tx, p_ex(EX), d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen(("some"|"any"), p_misc(DT|PDT), (DET | DET_PREDET), cs) => let val x = freshVar() in (
                SOME(DETP(fn N => (fn P => N (fn y => exists(x, conj(pred(y, [x]), P x)))), "some", p_misc(DT), DET, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen(("every" | "all"), p_misc(DT|PDT), (DET | DET_PREDET), cs) => let val x = freshVar() in (
                SOME(DETP(fn N => (fn P => N (fn y => forall(x, impl(pred(y, [x]), P x)))), "all", p_misc(DT), DET, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen("no", p_misc(DT|PDT), (DET | DET_PREDET), cs) => let val x = freshVar() in (
                SOME(DETP(fn N => (fn P => N (fn y => forall(x, impl(pred(y, [x]), neg(P x))))), "no", p_misc(DT), DET, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen(("a" | "the"), p_misc(DT|PDT), (DET | DET_PREDET), cs) => let val x = freshVar() in (
                SOME(DETP(fn N => (fn P => N (fn y => exists(x, conj(pred(y, [x]), P x)))), "a", p_misc(DT), DET, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
            ) end
    |   sen(tx, p, d, cs) => SOME(MISCP(tx, p, d, phraseOptionsToList(map(fn s => senToPhrase(s)) cs)))
    
and phraseOptionsToList(ps:(phrase option) list):phrase list =
    case ps of
        nil => nil
    |   NONE :: tail => phraseOptionsToList(tail)
    |   SOME(p) :: tail => p :: phraseOptionsToList(tail)
    
    
    

(* This function converts a phrase to a formula by applying different phrase functions to one another *)

fun parseSentence(ph:phrase):formula option =
    case ph of
        TV (f, tx, p, ROOT, cs) => let val s = findS(cs) in (
                case findEXP(cs) of
                    NONE => (let val obj = findO(cs) in (
                                SOME(parseNP(s) (fn x => parseNP(obj) (fn y => (parseTV(ph) y) x)))
                            ) end)
                |   SOME(_) => SOME(parseNP(s) (fn x => atomic(truth)))
            ) end
    |   NP (f, tx, p_q(p), ROOT, cs) => let val s = findS(cs) in (
                SOME(parseNP(s) (fn x => atomic(truth)))
            ) end
    |   NP (f, tx, p, ROOT, cs) => let val s = findS(cs) in (
                SOME(parseNP(s) (fn y => parseNP(ph) (fn x => pred(x, [y]))))
            ) end
    |   ADVP (f, tx, p, ROOT, cs) => let val s = findS(cs) in (
                SOME(parseNP(s) (parseADVP(ph)))
            ) end
    |   QP (f, tx, p, ROOT, cs) => let val s = findS(cs) in (
                 let val obj = findO(cs) in (
                     let val vo = findP(cs) in (
                        case vo of
                            NONE => NONE
                        |   SOME(v) => SOME(parseNP(s) (fn x => parseNP(obj) (fn y => (parseTV(ph) y) x)))
                    ) end
                ) end
                
            ) end
    |   _ => raise UNPARSABLE_SENTENCE
    



and parseADVP(ph:phrase):(term->formula) =
    case ph of
        ADVP(f, tx, p, d, NEGP(nf, _, _, _, _) :: cs) => (fn x => (fn y => (neg(x y)))) (parseADVP(ADVP(f, tx, p, d, cs)))
    |   ADVP(f, tx, p, d, _ :: cs) => parseADVP(ADVP(f, tx, p, d, cs))
    |   ADVP(f, _, _, _, nil) => f
    |   _ => raise WRONG_PHRASE_TYPE

and parseTVHelper(ph:phrase):(term->term->(term->formula)->formula) =
    case ph of
        TV(f, tx, p, d, NEGP(nf, _, _, _, _) :: cs) => (fn V => (fn x => (fn y => (fn P => (nf V x y P))))) (parseTVHelper(TV(f, tx, p, d, cs)))
    |   TV(f, "be", p, d, EXP(ef, _, _, _, _) :: cs) => ef (parseTVHelper(TV(f, "be", p, d, cs)))
    |   TV(f, tx, p, d, EXP(ef, _, _, _, _) :: cs) => (fn P => (fn v => (fn x => (fn y => (fn Q => (conj((P v y x Q), v y x Q))))))) ef (parseTVHelper(TV(f, tx, p, d, cs)))
    |   TV(f, tx, p, d, NP(oblf, obltx, obld, OBL, oblcs) :: cs) => (
                case findCASE(oblcs) of
                    NONE => parseTVHelper(TV(f, tx, p, d, cs))
                |   SOME(ADPP(adf, "by", adp, add, adcs)) => (fn Q => (fn x => (fn y => (fn P => (parseNP(NP(oblf, obltx, obld, OBL, oblcs))) (fn z => Q z y P)   )))) (parseTVHelper(TV(f, tx, p, d, cs)))
                |   SOME(ADPP(adf, adtx, adp, add, adcs)) => (fn x => (fn y => (fn P => ((parseTVHelper(TV(f, tx, p, d, cs))) x y ((adf (parseNP(NP(oblf, obltx, obld, OBL, oblcs)))) P)))))
                |   SOME(_) => raise ILLEGAL_STATE
            )
    |   TV(f, tx, p, d, VMOD(vmf, _, _, _, _) :: cs) => vmf (parseTVHelper(TV(f, tx, p, d, cs)))
    |   TV(f, tx, p, d, NP(iof, iotx, iop, IOBJ, iocs) :: cs) => (fn x => (fn y => (fn P => ((parseTVHelper(TV(f, tx, p, d, cs))) x y ((fn Q => (fn e => conj(pred(ID("{ev_GOL}"), [ID(iotx), e]), Q e))) P)))))
    |   TV(f, tx, p, d, QP(qf, _, _, ADVMOD, _) :: cs) => (qf (parseTVHelper(TV(f, tx, p, d, cs))))
    |   TV(f, tx, p, d, TV(vf, vtx, vp, CONJ, vcs) :: cs) => let val cco = findCC(vcs) in (
                let val conjvo = parseSentence(TV(vf, vtx, vp, ROOT, vcs)) in (
                    case conjvo of
                        NONE => parseTVHelper(TV(vf, vtx, vp, CONJ, vcs))
                    |   SOME(conjf) => case cco of
                            NONE => parseTVHelper(TV(f, tx, p, d, cs))
                        |   SOME(cc) => case cc of
                                MISCP("or", _, _, _) => parseTVHelper(TV(fn x => (fn y => (fn P => (disj(f x y P, vf x y P)))), tx, p, d, cs))
                            |   MISCP(_, _, _, _) => parseTVHelper(TV(fn x => (fn y => (fn P => (conj(f x y P, conjf)))), tx, p, d, cs))
                            |   _ => parseTVHelper(TV(f, tx, p, d, cs))
                ) end
            ) end
    |   TV(f, tx, p, d, MODP(mf, mtx, mp, AUX, mcs) :: cs) => mf (parseTVHelper(TV(f, tx, p, d, cs)))
    |   TV(f, tx, p, d, TV(xf, xtx, xp, XCOMP, xcs) :: cs) => (fn x => (fn y => (fn P => ((parseNP(findO(xcs))) (fn k => ((parseTVHelper(TV(xf, xtx, xp, XCOMP, xcs))) k y (fn z => conj(pred(ID("{ev_MOD}"), [ID(tx), z]), P z))))))))
    |   TV(f, tx, p, d, NP(xf, xtx, xp, XCOMP, xcs) :: cs) => (fn x => (fn y => (fn P => ((parseTVHelper(TV(f, tx, p, d, cs))) x y (fn z => (parseNP(NP(xf, xtx, xp, XCOMP, xcs))) (fn k => (conj(pred(ID("{ev_PRD}"), [k, z]), P z))))))))
    |   TV(f, tx, p, d, _ :: cs) => parseTVHelper(TV(f, tx, p, d, cs))
    |   TV(f, _, _, _, nil) => f
    |   _ => raise WRONG_PHRASE_TYPE

and parseTV(ph:phrase):(term->term->formula) = (fn x => (fn y => (parseTVHelper(ph) x y (fn z => pred(ID("{event}"), [z])))))

and parseNP(ph:phrase):(term->formula)->formula =
    case ph of
        NP(f, tx, p, d, AP(af, atx, ap, ad, acs) :: cs) => (af) (parseNP(NP(f, tx, p, d, cs)))
    |   NP(f, tx, p, d, NEGP(nf, _, _, _, _) :: cs) => (fn n => (fn P => neg(n P))) (parseNP(NP(f, tx, p, d, cs)))
    
    |   NP(f, tx, p, d, DETP(df, _, _, _, _) :: nil) => df (parseNP(NP(f, tx, p, d, nil)))
    |   NP(f, tx, p, d, DETP(df, dtx, dp, dd, dcs) :: cs) => parseNP(NP(f, tx, p, d, cs @ [DETP(df, dtx, dp, dd, dcs)])) (* this causes determiners to be parsed last (i.e., they are applied to the basic noun) such that all subsequent operations get the quantified noun as input *)
    
    |   NP(f, tx, p, d, POSSP(pf, _, _, _, _) :: nil) => pf (parseNP(NP(f, tx, p, d, nil)))
    |   NP(f, tx, p, d, POSSP(pf, ptx, pp, pd, pcs) :: cs) => parseNP(NP(f, tx, p, d, cs @ [POSSP(pf, ptx, pp, pd, pcs)]))
    
    |   NP(f, tx, p, d, NMODQ(qpf, _, _, _, _) :: nil) => qpf (parseNP(NP(f, tx, p, d, nil)))
    |   NP(f, tx, p, d, NMODQ(qpf, qptx, qpp, qpd, qpcs) :: cs) => parseNP(NP(f, tx, p, d, cs @ [NMODQ(qpf, qptx, qpp, qpd, qpcs)]))
    
    |   NP(f, tx, p, d, NP(nmf, nmtx, nmp, NMOD, nmcs) :: cs) => (case findCASE(nmcs) of 
                    NONE => parseNP(NP(f, tx, p, d, cs))
                |   SOME(ADPP(adf, "of", adp, add, adcs)) => let val e = freshVar() in (
                        (fn P => (parseNP(NP(f, tx, p, d, cs))) (fn x => conj(P x, (parseNP(NP(nmf, nmtx, nmp, NMOD, nmcs))) (fn y => exists(e, conj(pred(ID("{event}"), [e]), conj(pred(ID("{ev_PAG}"), [y, e]), conj(pred(ID("{ev_REL}"), [ID("{possess}"), e]), pred(ID("{ev_PPT}"), [x, e])))))))))
                    ) end
                |   SOME(_) => raise WRONG_PHRASE_TYPE
            )
    |   NP(f, tx, p, d, TV(vf, vtx, vp, ACL_RELCL, vcs) :: cs) => let val s = findS(vcs) in (
                let val obj = findO(vcs) in (
                    case (s, obj) of
                        (NP_VAR(_, _, _, _), NP_VAR(varf, _, _, _)) => (fn P => (fn Q => (conj((parseNP(NP(f, tx, p, d, cs))) (fn x => (P (fn y => (parseTV(TV(vf, vtx, vp, ACL_RELCL, vcs)) y) x))), (parseNP(NP(f, tx, p, d, cs))) (fn y => (Q y)))))) varf
                    |   (NP(sf, stx, sp, sd, scs), NP_VAR(_, _, _, _)) => (fn P => (fn Q => (conj(parseNP(s) (fn x => (P (fn y => (parseTV(TV(vf, vtx, vp, ACL_RELCL, vcs)) y) x))), P (fn y => (Q y)))))) (parseNP(NP(f, tx, p, d, cs)))
                    |   (NP_VAR(_, _, _, _), NP(objf, objtx, objp, objd, objcs)) => (fn P => (fn Q => (conj((parseNP(NP(f, tx, p, d, cs))) (fn x => (P (fn y => (parseTV(TV(vf, vtx, vp, ACL_RELCL, vcs)) y) x))), (parseNP(NP(f, tx, p, d, cs))) (fn y => (Q y)))))) (parseNP(obj))
                    |   (NP(sf, stx, sp, sd, scs), NP(objf, objtx, objp, objd, objcs)) => parseNP(NP(f, tx, p, d, cs))
                    |   (_, _) => raise ILLEGAL_STATE
                ) end
            ) end
    |   NP(f, tx, p, d, NP(nf, ntx, np, CONJ, ncs) :: cs) => let val cco = findCC(ncs) in (
                    case cco of
                        NONE => parseNP(NP(f, tx, p, d, cs))
                    |   SOME(cc) => case cc of
                            MISCP("or", _, _, _) => (fn P => (disj((parseNP(NP(f, tx, p, d, cs))) P, nf P)))
                        |   MISCP(_, _, _, _) => (fn P => (conj((parseNP(NP(f, tx, p, d, cs))) P, nf P)))
                        |   _ => parseNP(NP(f, tx, p, d, cs))
                    ) end
    |   NP(f, tx, p, d, _ :: cs) => parseNP(NP(f, tx, p, d, cs))
    |   NP(f, _, _, _, nil) => f
    |   NP_VAR(f, _, _, _) => f
    |   _ => raise WRONG_PHRASE_TYPE




(* these functions determine the type of a given modal word with respect to its negation state *)
fun handleMods(f:formula):formula =
    case f of
        impl(a, b) => impl(handleMods(a), handleMods(b))
    |   conj(a, b) => conj(handleMods(a), handleMods(b))
    |   disj(a, b) => disj(handleMods(a), handleMods(b))
    |   exists(a, b) => exists(a, handleMods(b))
    |   forall(a, b) => forall(a, handleMods(b))
    |   neg(a) => neg(handleMods(a))
    |   atomic(a) => f
    |   pred(ID("{ev_MOD}"), [ID(modal), x]) => pred(ID("{ev_MOD}"), [ID(determineModType(modal)), x])
    |   pred(t, ts) => pred(t, ts)

and determineModType(modal:string):string =
    case modal of
            ("can" | "could") => "{possibility}"
        |   ("may" | "allow") => "{permission}"
        |   "forbid" => "{prohibition}"
        |   ("must" | "should" | "shall" | "have" | "oblige" | "require" | "need") => "{obligation}"
        |   ("want" | "wish" | "would like" | "would love") => "{desire}"
        |   ("like" | "enjoy" | "prefer" | "love") => "{liking}"
        |   ("hate" | "dislike") => "{disliking}"
        |   _ => ""




(* Takes a term x and a formula and determines if these are the components of an event (i.e., x should be a variable and the formula should contain the predicate {event}(x)) *)
fun checkIsEvent(x:term, f:formula):bool =
    case x of
        ID(_) => false
    |   var(_) => (
        case f of
            impl(a, b) => checkIsEvent(x, a) orelse checkIsEvent(x, b)
        |   conj(a, b) => checkIsEvent(x, a) orelse checkIsEvent(x, b)
        |   disj(a, b) => checkIsEvent(x, a) orelse checkIsEvent(x, b)
        |   exists(a, b) => checkIsEvent(x, b)
        |   forall(a, b) => checkIsEvent(x, b)
        |   neg(a) => false
        |   atomic(a) => false
        |   pred(ID("{event}"), [ev]) => if ev = x then true else false
        |   _ => false)
    |   truth => false
    |   falsity => false




(* This function normalizes a formula such that it's easier to translate to SPARQL, i.e. it removes universal quantifiers and restricts negations to predicates and existential quantifiers, furthermore it handles modal verb classification *)
fun normalize(form:formula):formula =
    case form of
        pred(t, ts) => pred(t, ts)
        
    |   impl(a, b) => normalize(disj(neg(a), b))
    |   conj(a, atomic(truth)) => normalize(a)
    |   conj(atomic(truth), b) => normalize(b)
    |   conj(a, b) => conj(normalize(a), normalize(b))
    |   disj(a, atomic(falsity)) => normalize(a)
    |   disj(atomic(falsity), b) => normalize(b)
    |   disj(a, b) => disj(normalize(a), normalize(b))
    |   exists(x, f) => (case checkIsEvent(x, f) of
                true => exists(x, normalize(conj(handleMods(f), pred(ID("{ev_NEG}"), [ID("false"), x]))))
            |   false => exists(x, normalize(f)))
    |   forall(x, f) => neg(exists(x, normalize(neg(f))))
    |   neg(neg(f)) => normalize(f)
    |   neg(disj(a, b)) => normalize(conj(neg(a), neg(b)))
    |   neg(conj(a, b)) => normalize(disj(neg(a), neg(b)))
    |   neg(impl(a, b)) => normalize(conj(a, neg(b)))
    |   neg(forall(x, f)) => exists(x, normalize(neg(f)))
    |   neg(exists(x, f)) => (case checkIsEvent(x, f) of
                true => neg(exists(x, normalize(conj(handleMods(f), pred(ID("{ev_NEG}"), [ID("false"), x])))))
            |   false => neg(exists(x, normalize(f))))
    |   neg(f) => neg(normalize(f))
    |   atomic(t) => atomic(t)
    
    
    

(* This function transforms a list of 'a options to a list of 'a by removing all NONE values *)
fun flattenOptionList(xs:(('a option) list)):('a list) =
    case xs of
        nil => nil
    |   NONE :: tail => flattenOptionList(tail)
    |   SOME(x) :: tail => x :: flattenOptionList(tail)

(* These functions convert strings to POS/DEP values *)
fun posFromStr(pos:string):POS =
    case pos of
        "ADD" => p_n(ADD)
    |   "NN" => p_n(NN)
    |   "NNS" => p_n(NNS)
    |   "NNP" => p_n(NNP)
    |   "NNPS" => p_n(NNPS)
    |   "PRP" => p_n(PRP)
    |   "VB" => p_v(VB)
    |   "VBD" => p_v(VBD)
    |   "VBG" => p_v(VBG)
    |   "VBN" => p_v(VBN)
    |   "VBP" => p_v(VBP)
    |   "VBZ" => p_v(VBZ)
    |   "JJ" => p_adj(JJ)
    |   "JJR" => p_adj(JJR)
    |   "JJS" => p_adj(JJS)
    |   "CD" => p_adj(CD)
    |   "WRB" => p_q(WRB)
    |   "WP" => p_q(WP)
    |   "PRP$" => p_poss(PRP_POSS)
    |   "WP$" => p_poss(WP_POSS)
    |   "RB" => p_adv(RB)
    |   "RBR" => p_adv(RBR)
    |   "RBS" => p_adv(RBS)
    |   "EX" => p_ex(EX)
    |   "MD" => p_mod(MD)
    |   "CC" => p_misc(P_CC)
    |   "PDT" => p_misc(PDT)
    |   "DT" => p_misc(DT)
    |   "IN" => p_misc(IN)
    |   "WDT" => p_misc(WDT)
    |   "TO" => p_misc(TO)
    |   "POS" => p_misc(P_POS)
    |   "RP" => p_misc(RP)
    |   _ => raise ILLEGAL_POS

fun depFromStr(dep:string):DEP =
    case dep of
        "acl" => ACL
    |   "acl:relcl" => ACL_RELCL
    |   "advmod" => ADVMOD
    |   "appos" => APPOS
    |   "case" => CASE
    |   "ccomp" => CCOMP
    |   "conj" => CONJ
    |   "cop" => COP
    |   "dep" => DEP
    |   "expl" => EXPL
    |   "flat" => FLAT
    |   "list" => LIST
    |   "nsubj" => NSUBJ
    |   "nsubj:pass" => NSUBJ_PASS
    |   "obj" => OBJ
    |   "root" => ROOT
    |   "advcl" => ADVCL
    |   "amod" => AMOD
    |   "aux" => AUX
    |   "aux:pass" => AUX_PASS
    |   "cc" => CC
    |   "compound" => COMPOUND
    |   "compound:prt" => COMPOUND_PRT
    |   "det" => DET
    |   "det:predet" => DET_PREDET
    |   "fixed" => FIXED
    |   "iobj" => IOBJ
    |   "mark" => MARK
    |   "nmod" => NMOD
    |   "nmod:poss" => NMOD_POSS
    |   "nmod:tmod" => NMOD_TMOD
    |   "nummod" => NUMMOD
    |   "obl" => OBL
    |   "obl:tmod" => OBL_TMOD
    |   "obl:npmod" => OBL_NPMOD
    |   "vocative" => VOCATIVE
    |   "xcomp" => XCOMP
    |   _ => raise ILLEGAL_DEP




(* This functions gets a JSON encoded sentence and decodes it *)
fun jsonToSen(json:JSON.value):(sentence option) =
    case json of
        JSON.ARRAY [JSON.STRING tx, JSON.STRING pos, JSON.STRING dep, JSON.ARRAY cs] => SOME(sen(tx, (posFromStr pos), (depFromStr dep), flattenOptionList(JSONUtil.arrayMap jsonToSen (JSON.ARRAY cs))))
    |   _ => NONE

fun fromJSON(json:JSON.value):(sentence option) =
    case json of
        JSON.OBJECT [("code", code)] => jsonToSen(code)
    |   _ => NONE
    
(* These functions encode a formula as JSON for exchange with the web application *)
fun tToJSON(t:term):string =
    case t of
        var v => "[\"var\", \"" ^ v ^ "\"]"
    |   ID i => "[\"ID\", \"" ^ i ^ "\"]"
    |   truth => "[\"truth\"]"
    |   falsity => "[\"falsity\"]"

fun fToJSON(form:formula):string = 
    case form of
            pred (p, ts) => "[\"pred\", " ^ tToJSON(p) ^ ", [" ^ (foldl (fn (a, b) => if b <> "" then a ^ "," ^ b else a) "")(map (fn t => tToJSON(t)) ts) ^ "]]"
        |   impl (f1, f2) => "[\"impl\", " ^ fToJSON(f1) ^ ", " ^ fToJSON(f2) ^ "]"
        |   conj (f1, f2) => "[\"conj\", " ^ fToJSON(f1) ^ ", " ^ fToJSON(f2) ^ "]"
        |   disj (f1, f2) => "[\"disj\", " ^ fToJSON(f1) ^ ", " ^ fToJSON(f2) ^ "]"
        |   exists (i, f) => "[\"exists\", " ^ tToJSON(i) ^ ", " ^ fToJSON(f) ^ "]"
        |   forall (i, f) => "[\"forall\", " ^ tToJSON(i) ^ ", " ^ fToJSON(f) ^ "]"
        |   neg (f) => "[\"neg\", " ^ fToJSON(f) ^ "]"
        |   atomic (t) => tToJSON(t)

fun toJSON(form:formula):string = "{\"code\": " ^ fToJSON(form) ^ "}"
    



fun processInput(json:string):unit =
    let val sen_option = fromJSON(JSONParser.parse (JSONParser.openString json)) in (
        case sen_option of
                NONE => print("Could not parse JSON. Please check the syntax.\n")
            |   SOME(s) => (
                    let val phrase_option = senToPhrase(s) in (
                        case phrase_option of
                                NONE => print("An error occured while parsing the sentence.\n")
                            |   SOME(p) => (
                                    case parseSentence(p) of
                                            NONE => print("The parsed sentence could not be transformed to a formula.\n")
                                        |   SOME(f) => print(toJSON(normalize(f)))
                                )
                    ) end
                )
    ) end


fun main ():unit =
    case CommandLine.arguments () of
         []     => print("Error: Too few arguments.\n")
       | [json] => processInput(json)
       | args   => print("Error: Too many arguments.\n")

val _ = main ()
