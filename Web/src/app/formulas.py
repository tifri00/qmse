class ContextDecl:
    def __init__(self, x):
        if not isinstance(x, Variable):
            raise TypeError("Context variable " + str(x) + " should be of type Variable")

        self.x = x

    def __eq__(self, other):
        return other is not None and isinstance(other, ContextDecl) and self.x == other.x

    def __hash__(self):
        return hash(('x', self.x))

class ContextTruth(ContextDecl):
    def __init__(self, x):
        super().__init__(x)

    def __eq__(self, other):
        return other is not None and isinstance(other, ContextTruth) and self.x == other.x

    def __hash__(self):
        return hash(('x', self.x))

    def __str__(self):
        return str(self.x) + " : True"

class ContextFalsity(ContextDecl):
    def __init__(self, x):
        super().__init__(x)

    def __eq__(self, other):
        return other is not None and isinstance(other, ContextFalsity) and self.x == other.x

    def __hash__(self):
        return hash(('x', self.x))

    def __str__(self):
        return str(self.x) + " : False"

class ContextCount(ContextDecl):
    def __init__(self, x, n):
        super().__init__(x)
        self.n = n

    def __eq__(self, other):
        return other is not None and isinstance(other, ContextCount) and self.x == other.x and self.n == other.n

    def __hash__(self):
        return hash(('x', self.x, 'n', self.n))

    def __str__(self):
        return str(self.x) + " : Count(" + str(self.n) + ")"

class ContextVarCount(ContextDecl):
    def __init__(self, x):
        super().__init__(x)

    def __eq__(self, other):
        return other is not None and isinstance(other, ContextVarCount) and self.x == other.x

    def __hash__(self):
        return hash(('x', self.x))

    def __str__(self):
        return str(self.x) + " : Count(?)"

class Term:
    def __init__(self):
        pass

class Variable(Term):
    def __init__(self, v):
        super().__init__()

        self.v = v

    def __eq__(self, other):
        return other is not None and isinstance(other, Variable) and self.v == other.v

    def __hash__(self):
        return hash(('v', self.v))

    def __str__(self):
        return str(self.v)

class Identifier(Term):
    def __init__(self, i):
        super().__init__()

        self.i = i

    def __eq__(self, other):
        return other is not None and isinstance(other, Identifier) and self.i == other.i

    def __hash__(self):
        return hash(('i', self.i))

    def __str__(self):
        return str(self.i)

class Concept(Term):
    def __init__(self, iri):
        super().__init__()

        self.iri = iri

    def __str__(self):
        return "<" + str(self.iri) + ">"

    def __eq__(self, other):
        return other is not None and isinstance(other, Concept) and self.iri == other.iri

    def __hash__(self):
        return hash(('iri', self.iri))

class Formula:
    def __init__(self):
        pass

class Predicate(Formula):
    def __init__(self, p, args):
        super().__init__()

        self.p = p
        self.args = args

    def __str__(self):
        return str(self.p) + "(" + ", ".join(list(map(lambda x: str(x), self.args))) + ")"

class Conjunction(Formula):
    def __init__(self, f1, f2):
        super().__init__()

        self.f1 = f1
        self.f2 = f2

    def __str__(self):
        return "(" + str(self.f1) + " & " + str(self.f2) + ")"

class Disjunction(Formula):
    def __init__(self, f1, f2):
        super().__init__()

        self.f1 = f1
        self.f2 = f2

    def __str__(self):
        return "(" + str(self.f1) + " | " + str(self.f2) + ")"

class Exists(Formula):
    def __init__(self, t, f):
        super().__init__()

        self.t = t
        self.f = f

    def __str__(self):
        return "exists " + str(self.t) + ".(" + str(self.f) + ")"

class Negation(Formula):
    def __init__(self, f):
        super().__init__()

        self.f = f

    def __str__(self):
        return "~(" + str(self.f) + ")"

def removePredicateFromFormula(pred, formula):
    if isinstance(formula, Predicate):
        if formula == pred:
            return None

        return formula

    elif isinstance(formula, Conjunction) or isinstance(formula, Disjunction):
        f1 = removePredicateFromFormula(pred, formula.f1)
        f2 = removePredicateFromFormula(pred, formula.f2)

        if f1 is None:
            return f2
        elif f2 is None:
            return f1

        formula.f1 = f1
        formula.f2 = f2
        return formula

    elif isinstance(formula, Exists) or isinstance(formula, Negation):
        f = removePredicateFromFormula(pred, formula.f)

        if f is None:
            return None

        formula.f = f
        return formula
