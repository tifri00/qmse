import eml_parser

def getEmailData(file):
    file.data.open(mode="rb")
    content = file.data.read()
    file.data.close()

    data = eml_parser.eml_parser.decode_email_b(content, True)

    subject = ""
    sender = ""
    recipients = ""
    cc = ""
    bcc = ""
    datetime = ""

    if "header" in data:
        subject = data["header"]["subject"] if "subject" in data["header"] else None
        sender = data["header"]["from"] if "from" in data["header"] else None
        recipients = data["header"]["to"] if "to" in data["header"] else None
        cc = data["header"]["cc"] if "cc" in data["header"] else None
        bcc = data["header"]["bcc"] if "bcc" in data["header"] else None
        datetime = data["header"]["date"] if "date" in data["header"] else None

    text = data["body"][0]["content"] if "body" in data and len(data["body"]) > 0 and "content" in data["body"][
        0] else None

    result = {
        "subject": subject,
        "from": sender,
        "to": recipients,
        "cc": cc,
        "bcc": bcc,
        "datetime": datetime,
        "text": text,
    }

    return result