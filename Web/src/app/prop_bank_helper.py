from .formulas import *
import os
from nltk.corpus import wordnet as wn
import xml.etree.ElementTree as ET
from . import constants
import itertools
from Levenshtein import distance as lev_distance
import re

FRAMES_FOLDER_NAME = "frames"

frames = []
vn_fn = None
pb_vn = None
tpp_data = None


def init():
    global frames
    global vn_fn
    global pb_vn
    global tpp_data

    frames = os.listdir(FRAMES_FOLDER_NAME)

    vn_fn = ET.parse(constants.SUPPORT_FILES_FOLDER + "semlink-VN-FNRoleMapping.xml")
    if vn_fn:
        vn_fn = vn_fn.getroot()

    pb_vn = ET.parse(constants.SUPPORT_FILES_FOLDER + "semlink-PB-VNRoleMapping.xml")
    if pb_vn:
        pb_vn = pb_vn.getroot()

    tpp_data = ET.parse(constants.SUPPORT_FILES_FOLDER + "tpp.xml")
    if tpp_data:
        tpp_data = tpp_data.getroot().findall("entry")


def findTPPEntryForPreposition(p):
    for entry in tpp_data:
        if entry.find("S/hg/hw[.='" + p + "']") is None:
            continue

        return entry

    return None


def findVNClassesByLemma(l):
    pred = pb_vn.find("predicate[@lemma='" + l + "']")
    argmaps = []
    if pred is None:
        synsets = wn.synsets(l, pos=wn.VERB)
        if not synsets or len(synsets) < 1:
            return []

        lemmas = []
        for synset in synsets:
            lemmas.extend(list(map(lambda lemma: str(lemma.name()), synset.lemmas())))
        lemmas = set(lemmas)

        for lemma in lemmas:
            pred = pb_vn.find("predicate[@lemma='" + lemma + "']")

            if pred is not None:
                argmaps.extend(list(map(lambda arg: (lemma, arg), pred.findall("argmap"))))

    else:
        argmaps.extend(list(map(lambda m: (l, m), pred.findall("argmap"))))

    return list(map(lambda m: (m[0], m[1].attrib["vn-class"].split("-", 1)[0]), argmaps))


def findFNFramesByVNClasses(vn_classes):
    result = []

    for vn_class in vn_classes:
        clss = vn_fn.findall("vncls[@class='" + vn_class[1] + "']")
        for cls in clss:
            result.append((vn_class[0], vn_class[1], cls.attrib["fnframe"]))

    return result


def findRolesByTPPEntryAndFNFrame(entry, fn_frame):
    result = []

    frfes = entry.findall(".//frfes")
    for frfe in frfes:
        roles = frfe.text.replace(" ", "").split(";")
        result.extend(list(map(lambda role: role.split(":")[1],
                               filter(lambda role: role.split(":")[0] in [fn_frame, "Event"], roles))))

    return result


def mapRoleMapToVNRoleMap(roles):
    result = {}

    evt_cls = vn_fn.find("vncls[@fnframe='Event']")
    evt_rolemap = {}
    evt_roles = evt_cls.findall("roles/role")
    for evt_role in evt_roles:
        evt_rolemap[evt_role.attrib["fnrole"]] = evt_role.attrib["vnrole"]

    for (l, vn_class, fn_frame) in roles:
        if not vn_class in result:
            result[(l, vn_class)] = []

        clss = vn_fn.findall("vncls[@class='" + vn_class + "']")
        rls = []
        for cls in clss:
            if cls.attrib["fnframe"] == fn_frame:
                rls = cls.findall("roles/role")
                break

        fnroles = roles[(l, vn_class, fn_frame)]

        for role in rls:
            fnrole = role.attrib["fnrole"]
            if fnrole in fnroles and fnrole not in result[(l, vn_class)]:
                result[(l, vn_class)].append(role.attrib["vnrole"])
                fnroles.remove(fnrole)

        if len(fnroles) > 0:
            for fnrole in fnroles:
                if fnrole in evt_rolemap and evt_rolemap[fnrole] not in result[(l, vn_class)]:
                    result[(l, vn_class)].append(evt_rolemap[fnrole])

    return result


def mapVNRoleMapToPBArgNums(vn_roles):
    result = {}

    for (l, vn_class) in vn_roles:
        pred = pb_vn.find("predicate[@lemma='" + l + "']")
        if pred is None:
            continue

        argmap = pred.find("argmap[@vn-class='" + vn_class + "']")
        if argmap is None:
            continue

        pb_roleset = argmap.attrib["pb-roleset"]
        if not pb_roleset in result:
            result[pb_roleset] = []

        roles = list(filter(lambda r: r.attrib["vn-theta"] in vn_roles[(l, vn_class)], argmap.findall("role")))
        rolemap = {}
        for role in roles:
            rolemap[role.attrib["vn-theta"]] = role.attrib["pb-arg"]

        for role in vn_roles[(l, vn_class)]:
            if role in rolemap:
                result[pb_roleset].append(rolemap[role])
            elif role in ["Location", "Time"]:
                result[pb_roleset].append(role)

    return result


def mapPBArgnumsToArgtypes(argnums):
    result = []

    for frame in argnums:
        lemma, nr = frame.split(".", 1)
        rolesets = getRolesetsByLemma(lemma)

        if not frame in rolesets:
            continue

        for n in argnums[frame]:
            if n in rolesets[frame]:
                result.append(rolesets[frame][n])
            elif n == "Location":
                result.append("loc")
            elif n == "Time":
                result.append("tmp")

    return list(set(result))


def getRolesetsByLemma(lemma):
    frame = list(filter(lambda f: f == lemma + ".xml", frames))
    frame = frame[0] if frame and len(frame) > 0 else None

    if not frame:
        return None

    tree = ET.parse("frames/" + frame)
    tree = tree.find("./predicate[@lemma='" + lemma + "']")
    rolesets = tree.findall("roleset")

    result = {}

    for roleset in rolesets:
        tmp = {}
        roles = roleset.findall("roles/role")
        for role in roles:
            f = role.attrib["f"]
            tmp[role.attrib["n"]] = f.lower()
        result[roleset.attrib["id"]] = tmp

    return result


def findPossibleRolesByPrepositionAndLemma(prep, lemma):
    tpp = findTPPEntryForPreposition(prep)
    # vn classes for a given lemma taken from the pb-vn map
    vn_classes = findVNClassesByLemma(lemma)
    print("VN classes: " + str(vn_classes))

    # find fn frames corresponding to the vn classes
    fn_frames = findFNFramesByVNClasses(vn_classes)
    print("FN frames: " + str(fn_frames))

    # the fn roles for the fn frames can be determined via tpp
    roles = {}
    for (l, vn_frame, frame) in fn_frames:
        fn_roles = findRolesByTPPEntryAndFNFrame(tpp, frame)
        roles[(l, vn_frame, frame)] = fn_roles
    print("FN roles: " + str(roles))

    # the role map can now be mapped to a vn role map containing vn classes with the corresponding roles
    vn_roles = mapRoleMapToVNRoleMap(roles)
    print("VN roles: " + str(vn_roles))

    # the pb argnums can be determined via the pb vn map
    argnums = mapVNRoleMapToPBArgNums(vn_roles)
    print("PB argnums: " + str(argnums))

    # the pb arg types can be determined via the pb roleset
    argtypes = mapPBArgnumsToArgtypes(argnums)
    print("PB arg types: " + str(argtypes))

    return argtypes


def mapEventPlaceholderToPBRole(placeholder):
    if not placeholder in constants.EVENT_PLACEHOLDER_MAPPINGS:
        return ""

    return constants.EVENT_PLACEHOLDER_MAPPINGS[placeholder]


def mapPBRoleToEventPlaceholder(role):
    if not role in constants.EVENT_PLACEHOLDER_MAPPINGS.values():
        return ""

    return list(constants.EVENT_PLACEHOLDER_MAPPINGS.keys())[list(constants.EVENT_PLACEHOLDER_MAPPINGS.values()).index(role)]


def rateRoleCombination(combination, rolesets):
    result = 0

    for roleset in rolesets:
        res = 0
        used = []
        consider = True
        denominator = len(rolesets[roleset])

        for role in combination:
            cur = combination[role]
            isTmpOrLoc = cur in ["tmp", "loc"]
            if isTmpOrLoc or cur in rolesets[roleset].values():
                if cur in used and cur in ["pag", "ppt"]:
                    consider = False
                    break
                res += 1
                if isTmpOrLoc:
                    denominator += 1
                used.append(cur)

        if not consider:
            continue

        if denominator == 0:
            res = 0
        else:
            res = res / denominator

        if res > result:
            result = res

    return result


def determineArgTypes(formula, args, pagBound, pptBound, rel):
    argms = args[constants.EVENT_ARGM_PLACEHOLDER] if constants.EVENT_ARGM_PLACEHOLDER in args else []

    if len(argms) == 0:
        return

    rolesets = getRolesetsByLemma(rel)

    pag = mapEventPlaceholderToPBRole(constants.EVENT_PAG_PLACEHOLDER)
    ppt = mapEventPlaceholderToPBRole(constants.EVENT_PPT_PLACEHOLDER)

    if rolesets:
        for roleset in rolesets:
                if pagBound:
                    rolesets[roleset] = {k: v for k, v in rolesets[roleset].items() if v != pag}
                if pptBound:
                    rolesets[roleset] = {k: v for k, v in rolesets[roleset].items() if v != ppt}

    print("Rolesets: " + str(rolesets))

    possibleRoles = {}

    for pred in argms:
        case = pred.args[1].i

        possibleRoles[pred] = findPossibleRolesByPrepositionAndLemma(case, rel)

        pred.args = pred.args[::2]

    k, v = zip(*possibleRoles.items())
    combinations = [dict(zip(k, v)) for v in itertools.product(*v)]

    ratings = []

    for i, combination in enumerate(combinations):
        ratings.append(0)
        rating = rateRoleCombination(combination, rolesets)

        if rating > ratings[i]:
            ratings[i] = rating

    ratedCombinations = list(zip(combinations, ratings))

    bestCombination = None
    bestRating = 0
    for ratedCombination in ratedCombinations:
        if not bestCombination or ratedCombination[1] > bestRating:
            bestCombination = ratedCombination[0]
            bestRating = ratedCombination[1]

    if bestCombination:
        for pred in bestCombination:
            role = bestCombination[pred]
            placeholder = mapPBRoleToEventPlaceholder(role)
            if role in ["pag", "ppt"]:
                formula = removePredicateFromFormula(pred, formula)

                existingPreds = args[constants.EVENT_PAG_PLACEHOLDER if role == "pag" else constants.EVENT_PPT_PLACEHOLDER]
                if existingPreds and len(existingPreds) == 1 and isinstance(existingPreds[0], Predicate):
                    existingPreds[0].args[1] = pred.args[1]
            else:
                pred.p = Concept(constants.EVENT_IRIS[placeholder])
    else:
        # TODO: somehow handle the case that we have no idea what the prepositions mean (probably via keywords)
        pass


def findRoleNamesByLemma(lemma):
    result = {}

    vn_classes = findVNClassesByLemma(lemma)

    fn_frames = findFNFramesByVNClasses(vn_classes)

    frame = None
    best_score = None
    for (l, vn_class, fn_frame) in fn_frames:
        score = lev_distance(lemma, fn_frame)
        if best_score is None or score < best_score:
            best_score = score
            frame = (vn_class, fn_frame)

    if frame is None:
        return None

    roleNames = list(map(lambda r: (r.attrib["fnrole"], r.attrib["vnrole"]), vn_fn.findall("vncls[@class='" + str(frame[0]) + "'][@fnframe='" + str(frame[1]) + "']/roles/role")))

    argmaps = pb_vn.findall("predicate[@lemma='" + str(lemma) + "']/argmap")

    for argmap in argmaps:
        if re.match(frame[0] + "(-.*)?", argmap.attrib["vn-class"]):
            roles = argmap.findall("role")
            for role in roles:
                mapping = next(filter(lambda rn: rn[1] == role.attrib["vn-theta"], roleNames), None)
                if mapping:
                    result[role.attrib["pb-arg"]] = mapping[0]

    rolesets = getRolesetsByLemma(lemma)

    if rolesets:
        for roleset in rolesets:
            for argnum in rolesets[roleset]:
                if argnum in result:
                    rolename = rolesets[roleset][argnum]
                    try:
                        index = list(constants.EVENT_PLACEHOLDER_MAPPINGS.values()).index(rolename)
                        placeholder = list(constants.EVENT_PLACEHOLDER_MAPPINGS.keys())[index]
                    except ValueError:
                        continue

                    result[placeholder] = result.pop(argnum)

    return result
