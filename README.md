# Quality Management Search Engine (QMSE)

Implementation of the Quality Management Search Engine (QMSE), a document management system with an integrated natural language based semantic search engine, based on my bachelor thesis.

## Prerequisites

### Triple Store Setup

To store and retrieve data, QMSE needs a triple store. This store has to support SPARQL querying via a HTTP endpoint. Additionally, the requests and responses have to be in the correct format (expressed as JSON). QMSE was designed and tested with the Blazegraph JAR-file (available [here](https://github.com/blazegraph/database/releases/tag/BLAZEGRAPH_2_1_6_RC)) which is why Blazegraph is the recommended triple store for running QMSE. It should also work with Virtuoso (not tested extensively).

In [`/Web/src/app/constants.py`](/Web/src/app/constants.py) the address of the SPARQL endpoint can be configured (default "http://localhost:9999/blazegraph/namespace/qmse/sparql"). When running Blazegraph locally, creating a namespace called "qmse" should suffice to be able to keep the endpoint's address unchanged (the default port of Blazegraph is 9999).

### Ontology Setup

To be able to use QMSE, the Yago3 ontology (available [here](https://yago-knowledge.org/downloads/yago-3)), as well as the QMSE ontology (available [in this repo](/ontology/qmse.owl)) have to be loaded into the triple store.

## Running QMSE

If everything is set up correctly, QMSE can be run by first starting the triple store and then the web application.

## Screenshots

To give an impression of how QMSE works, the following screenshots show the general UI and some exemplary query results.

<img src="Screenshots/start_page.png" />
This is the main screen of QMSE where questions can be entered in the search input in the top.
<br>
<br>
<br>
<img src="Screenshots/polar_positive.png" />
Polar questions (Yes/No questions) are answered with a short answer, i.e., "Yes" or "No", as well as a tabular answer with the relevant data (in this case the documents created by the user "admin").
<br>
<br>
<br>
<img src="Screenshots/count_question.png" />
When asking for quantities, there is a short answer that displays the number, as well as a tabular answer with the relevant data (in this case the documents created by the user "DemoUser").
<br>
<br>
<br>
<img src="Screenshots/general_question_self.png" />
A general question asking for documents created by the current user is answered by a tabular answer with the respective documents.
<br>
<br>
<br>
