import stanza

from . import settings_utils
from .constants import *

import copy

nlp = None

class SentenceTree:
    def __init__(self, root, childs):
        self.root = root
        self.childs = childs

    def __str__(self):
        return self.prettyPrint()

    def prettyPrint(self):
        result = self.root.lemma + " (" + self.root.xpos + ", " + self.root.deprel + ")\n"

        if not self.childs or len(self.childs) == 0:
            return result

        ind = int(len(result) / 2)

        for child in self.childs:
            result += " " * ind + "|\n"
            childStr = child.prettyPrint()
            lines = childStr.split("\n")
            result += " " * ind + "--" + lines[0] + "\n"
            for line in lines[1:]:
                if len(line) > 0:
                    result += " " * ind + "|" + line + "\n"

        return result

    def __eq__(self, other):
        if not other or not isinstance(other, SentenceTree):
            return False

        return self.root.lemma == other.root.lemma and self.root.xpos == other.root.xpos and self.root.deprel == other.root.deprel

def initNLP():
    global nlp
    nlp = stanza.Pipeline(lang=settings_utils.getCurLang(), processors='tokenize,pos,lemma,depparse,ner')

def findRoot(sentence):
    return next(filter(lambda word: word.deprel == "root", sentence.words), None)


def processChilds(sentence, id):
    childs = list(filter(lambda word: word.head == id, sentence.words))

    childs = list(map(lambda word: SentenceTree(word, processChilds(sentence, word.id)), childs))

    return childs


def sentenceToTree(sentence):
    root = findRoot(sentence)

    childs = processChilds(sentence, root.id)

    return SentenceTree(root, childs)


def nlpToTree(doc):
    result = []
    for sentence in doc.sentences:
        result.append(sentenceToTree(sentence))

    return result


def find(sentence, pos, deps):
    if not sentence or not isinstance(sentence, SentenceTree) or (pos is None and (deps is None or len(deps) == 0)):
        return None

    if (pos is None or sentence.root.xpos == pos) and ((deps is None or len(deps) == 0) or sentence.root.deprel in deps):
        return sentence.root

    for child in sentence.childs:
        res = find(child, pos, deps)
        if res:
            return res

    return None


def removePunctuation(sentence):
    if not sentence or not isinstance(sentence, SentenceTree):
        return None

    if sentence.root.deprel == "punct":
        return None

    childs = []

    for child in sentence.childs:
        res = removePunctuation(child)
        if res:
            childs.append(res)

    return SentenceTree(sentence.root, childs)


def handleMWE(sentence):
    if not sentence or not isinstance(sentence, SentenceTree):
        return None

    root = copy.deepcopy(sentence.root)
    childs = []

    for child in sentence.childs:
        if child.root.deprel in ["flat", "fixed"]:
            root.text += " " + child.root.text
            root.lemma += " " + child.root.lemma
        elif child.root.deprel == "compound":
            root.text = child.root.text + " " + root.text
            root.lemma = child.root.lemma + " " + root.lemma
        elif (sentence.root.xpos in ADV or sentence.root.xpos in ADJ) and child.root.xpos == "WRB" and child.root.deprel == "advmod" and child.root.lemma in TERMS[settings_utils.getCurLang()]["how"]:
            root.text = child.root.text + " " + root.text
            root.lemma = child.root.lemma + " " + root.lemma
            root.xpos = "WRB"
            root.deprel = "advmod"
        elif child.root.xpos == "MD" and child.root.deprel == "aux" and root and len(list(filter(lambda c: c.root.deprel == "xcomp", sentence.childs))) > 0:
            root.text = child.root.text + " " + root.text
            root.lemma = child.root.lemma + " " + root.lemma
        elif child.root.lemma == "first" and root.lemma == "name":
            root.text = "forename"
            root.lemma = "forename"
        elif child.root.lemma == "last" and root.lemma == "name":
            root.text = "surname"
            root.lemma = "surname"
        else:
            childs.append(handleMWE(child))

    return SentenceTree(root, childs)


def preprocess(sentences):
    result = []

    for sentence in sentences:
        result.append(removePunctuation(handleMWE(sentence)))

    return result


def getSentenceTrees(query):
    doc = nlp(query)

    sentences = nlpToTree(doc)

    return preprocess(sentences), list(map(lambda s: s.text, doc.sentences))
