from .formulas import *

QMSE_ONTOLOGY_IRI = "http://localhost:9999/qmse"
QMSE_DATA_IRI = "http://localhost:9999/qmse-data"

SPARQL_ENDPOINT = "http://localhost:9999/blazegraph/namespace/qmse/sparql"

SUPPORT_FILES_FOLDER = "support-files/"
PARSER_PATH = SUPPORT_FILES_FOLDER + "parsing"
WN_MAPPINGS_PATH = SUPPORT_FILES_FOLDER + "mapping_wordnet.json"

NOUN = ["NN", "NNS", "NNP", "NNPS", "PRP"]
VERB = ["VB", "VBD", "VBG", "VBN", "VBP", "VBZ"]
ADJ = ["JJ", "JJR", "JJS", "CD"]
Q = ["WRB", "WP"]
POSS = ["PRP:POSS", "WP:POSS"]
ADV = ["RB", "RBR", "RBS"]

TERMS = {
    "en": {
        "how": ["how"],
    },
}

EVENT_PLACEHOLDER_MAPPINGS = {
    "{ev_PAG}": "pag",
    "{ev_PPT}": "ppt",
    "{ev_COM}": "com",
    "{ev_LOC}": "loc",
    "{ev_DIR}": "dir",
    "{ev_GOL}": "gol",
    "{ev_MNR}": "mnr",
    "{ev_TMP}": "tmp",
    "{ev_EXT}": "ext",
    "{ev_REC}": "rec",
    "{ev_PRD}": "prd",
    "{ev_PRP}": "prp",
    "{ev_CAU}": "cau",
    "{ev_MOD}": "mod",
}

POSSESS_PLACEHOLDER = "{possess}"
DESCR_PLACEHOLDER = "{descr}"

EVENT_PLACEHOLDER = "{event}"
EVENT_PAG_PLACEHOLDER = "{ev_PAG}"
EVENT_PPT_PLACEHOLDER = "{ev_PPT}"
EVENT_COM_PLACEHOLDER = "{ev_COM}"
EVENT_LOC_PLACEHOLDER = "{ev_LOC}"
EVENT_DIR_PLACEHOLDER = "{ev_DIR}"
EVENT_GOL_PLACEHOLDER = "{ev_GOL}"
EVENT_MNR_PLACEHOLDER = "{ev_MNR}"
EVENT_TMP_PLACEHOLDER = "{ev_TMP}"
EVENT_EXT_PLACEHOLDER = "{ev_EXT}"
EVENT_REC_PLACEHOLDER = "{ev_REC}"
EVENT_PRD_PLACEHOLDER = "{ev_PRD}"
EVENT_PRP_PLACEHOLDER = "{ev_PRP}"
EVENT_CAU_PLACEHOLDER = "{ev_CAU}"
EVENT_MOD_PLACEHOLDER = "{ev_MOD}"
EVENT_REL_PLACEHOLDER = "{ev_REL}"
EVENT_ARGM_PLACEHOLDER = "{ev_ARGM}"

EVENT_NEG_PLACEHOLDER = "{ev_NEG}"

EVENT_IRIS = {
    EVENT_PAG_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_pag",
    EVENT_PPT_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_ppt",
    EVENT_COM_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_com",
    EVENT_LOC_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_loc",
    EVENT_DIR_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_dir",
    EVENT_GOL_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_gol",
    EVENT_MNR_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_mnr",
    EVENT_TMP_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_tmp",
    EVENT_EXT_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_ext",
    EVENT_REC_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_rec",
    EVENT_PRD_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_prd",
    EVENT_PRP_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_prp",
    EVENT_CAU_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_cau",
    EVENT_MOD_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_mod",
    EVENT_REL_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_rel",
    EVENT_NEG_PLACEHOLDER: QMSE_ONTOLOGY_IRI + "#ev_neg",
    "{event}": QMSE_ONTOLOGY_IRI + "#event",
}

MOD_IRIS = {
    "{possibility}": QMSE_ONTOLOGY_IRI + "#mod_possibility",
    "{permission}": QMSE_ONTOLOGY_IRI + "#mod_permission",
    "{prohibition}": QMSE_ONTOLOGY_IRI + "#mod_prohibition",
    "{obligation}": QMSE_ONTOLOGY_IRI + "#mod_obligation",
    "{desire}": QMSE_ONTOLOGY_IRI + "#mod_desire",
    "{liking}": QMSE_ONTOLOGY_IRI + "#mod_liking",
    "{disliking}": QMSE_ONTOLOGY_IRI + "#mod_disliking",
    "{neutral}": QMSE_ONTOLOGY_IRI + "#mod_neutral",
    "{fact}": QMSE_ONTOLOGY_IRI + "#mod_fact",
}

DESCR_IRI = QMSE_ONTOLOGY_IRI + "#descr"

USER_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_user_110741590")
USERNAME_CONCEPT = Concept(QMSE_ONTOLOGY_IRI + "#username")
LAST_NAME_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_surname_106336904")
FIRST_NAME_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_first_name_106337307")
EMAIL_ADDRESS_CONCEPT = Concept(QMSE_ONTOLOGY_IRI + "#email-address")

HOLD_CONCEPT = Concept("https://w3id.org/cevo/Hold")
TALK_CONCEPT = Concept("https://w3id.org/cevo/Talk")
CREATE_CONCEPT = Concept("https://w3id.org/cevo/Create")
SEND_CONCEPT = Concept("https://w3id.org/cevo/Send")

EMAIL_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_electronic_mail_106279326")
SENDER_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_sender_110578762")
RECIPIENT_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_recipient_109627906")
SUBJECT_CONCEPT = Concept(QMSE_ONTOLOGY_IRI + "#email-subject")
EMAIL_DATETIME_CONCEPT = Concept(QMSE_ONTOLOGY_IRI + "#email-send-time")
TEXT_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_text_106387980")

DOCUMENT_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_document_106470073")
CREATOR_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_creator_109614315")
DOCUMENT_CREATION_DATETIME_CONCEPT = Concept(QMSE_ONTOLOGY_IRI + "#document-create-time")
TITLE_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_title_106345993")
DESCRIPTION_CONCEPT = Concept("http://yago-knowledge.org/resource/wordnet_description_106724763")

LABELLING_RELATIONS = "|".join([str(Concept(DESCR_IRI)), "rdfs:label", "skos:prefLabel", "skos:altLabel"])
