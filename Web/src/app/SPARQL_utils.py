import json
from . import constants
import requests
from .formulas import *

prefixes = ["PREFIX skos: <http://www.w3.org/2004/02/skos/core#>",
            "PREFIX yago: <http://yago-knowledge.org/resource/>",
            "PREFIX qmse: <" + constants.QMSE_ONTOLOGY_IRI + "#>",
            "PREFIX cevo: <https://w3id.org/cevo/>",
            "PREFIX cevov: <https://w3id.org/cevo/verb/>",
]


def execQuery(query):
    params = {
        "query": "\n".join(prefixes) + "\n" + query,
        "format": "json",
    }

    response = requests.get(constants.SPARQL_ENDPOINT, params=params)

    return json.loads(response.text) if response.status_code == 200 else None


def execUpdate(query):
    data = {
        "update": "\n".join(prefixes) + "\n" + str(query),
    }

    requests.post(constants.SPARQL_ENDPOINT, data=data)


def insertTriples(triples):
    query = "INSERT DATA {\n"

    for triple in triples:
        query += str(triple[0]) + " " + str(triple[1]) + " " + str(triple[2]) + ".\n"

    query += "}"

    data = {
        "update": "\n".join(prefixes) + "\n" + str(query),
    }

    response = requests.post(constants.SPARQL_ENDPOINT, data=data)

    return response.status_code == 200


def getVars(result):
    return result['head']['vars']


def getMatrix(queryResult, typed = False):
    result = []

    if not queryResult:
        return result

    for binding in queryResult["results"]["bindings"]:
        line = []
        for var in queryResult["head"]["vars"]:
            value = binding[var]["value"] if var in binding else None

            if value is not None and typed:
                type = binding[var]["type"]

                if type == "uri":
                    value = Concept(value)
                else:
                    value = Identifier(value)

            line.append(value)

        result.append(line)

    return result
