from .stanza_helper import SentenceTree
from .formulas import *
import json
from json import JSONDecodeError

def sentenceTreeToJSONHelper(sentence):
    if not isinstance(sentence, SentenceTree):
        raise TypeError("The argument is not a valid sentence tree")

    lemma = sentence.root.lemma
    xpos = sentence.root.xpos
    dep = sentence.root.deprel
    childs = sentence.childs

    return "[\"" + lemma + "\", \"" + xpos + "\", \"" + dep + "\", [" + ", ".join(list(map(lambda c: sentenceTreeToJSONHelper(c), childs))) + "]]"

def sentenceTreeToJSON(sentence):
    if not isinstance(sentence, SentenceTree):
        raise TypeError("The argument is not a valid sentence tree")

    return "{\"code\": " + sentenceTreeToJSONHelper(sentence) + "}"


def JSONToTerm(term):
    if not isinstance(term, list) or len(term) != 2:
        raise ValueError("JSON cannot transformed to a valid formula.")

    if term[0] == "var":
        return Variable(term[1])
    elif term[0] == "ID":
        return Identifier(term[1])
    else:
        raise ValueError("JSON cannot transformed to a valid formula.")


def JSONToFormulaHelper(formula):
    if not isinstance(formula, list) or len(formula) < 1:
        raise ValueError("JSON cannot transformed to a valid formula.")

    formulaType = formula[0]

    if formulaType == "pred" and len(formula) == 3:
        return Predicate(JSONToTerm(formula[1]), list(map(lambda x: JSONToTerm(x), formula[2])))
    elif formulaType == "conj" and len(formula) == 3:
        return Conjunction(JSONToFormulaHelper(formula[1]), JSONToFormulaHelper(formula[2]))
    elif formulaType == "disj" and len(formula) == 3:
        return Disjunction(JSONToFormulaHelper(formula[1]), JSONToFormulaHelper(formula[2]))
    elif formulaType == "exists" and len(formula) == 3:
        return Exists(JSONToTerm(formula[1]), JSONToFormulaHelper(formula[2]))
    elif formulaType == "neg" and len(formula) == 2:
        return Negation(JSONToFormulaHelper(formula[1]))
    else:
        raise ValueError("JSON cannot transformed to a valid formula.")


def JSONToFormula(jsn):
    try:
        jsonObj = json.loads(jsn)
    except JSONDecodeError:
        return None

    if not "code" in jsonObj:
        raise ValueError("JSON cannot transformed to a valid formula.")

    return JSONToFormulaHelper(jsonObj["code"])
