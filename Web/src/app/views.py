from django.shortcuts import render
import django_tables2 as tables

from login import login_utils
from files.models import File
from .tables import FileTable, DynamicColumnsTable, HTMLColumn
from . import search_utils
from .SearchResult import SearchResult


# Create your views here.

class TableView(tables.SingleTableView):
    table_class = FileTable
    queryset = File.objects.all()
    template_name = "index.html"


def index(request):
    if login_utils.login_required(request):
        return login_utils.redirect_login(request)

    if request.method == "POST":
        return handleSearch(request)

    fileTable = FileTable(File.objects.all())

    return render(request, 'index.html', {'fileTable': fileTable})


def generateSearchResultTable(sr):
    columns = []
    for column in sr.data:
        columns.append((column, HTMLColumn()))

    data = []
    for i in range(sr.dataLines):
        line = {}
        for key in sr.data:
            line[key] = sr.data[key][i]
        data.append(line)

    table = DynamicColumnsTable(
                column_specs=tuple(columns),
                data=data)

    return table


def handleSearch(request):
    query = request.POST.get("q")

    searchResults = search_utils.search(query, request.user)

    searchResults = list(map(lambda sr: SearchResult(sr.title,
                                                     generateSearchResultTable(sr),
                                                     sr.dataLines,
                                                     sr.sentenceString),
                             searchResults))

    return render(request, 'index.html', {'query': query, 'searchResults': searchResults})
