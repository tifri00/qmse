from django.db import models
from django.conf import settings
from files.models import File

# Create your models here.

class ExtractedUserInfo(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    concept = models.CharField(max_length=512, unique=True, default="")
    isUserIndividual = models.BooleanField(default=False)

class ExtractedFileInfo(models.Model):
    file = models.ForeignKey(File, on_delete=models.CASCADE)
    concept = models.CharField(max_length=512, unique=True, default="")
