from django.template import Context, Template
from app import email_util

def getFileRendering(file):
    t = Template("<h1>There's no preview available for this file type.</h1>")
    c = Context({})

    if file.type == "application/pdf":
        t = Template("{% load static %}<embed class='file-preview-content' src='{% get_media_prefix %}{{ file.data }}' type='{{ file.type }}'>")
        c = Context({'file': file})
    elif file.type == "message/rfc822":
        t = Template("<div class='file-preview-content'>{% include 'eml_preview.html' %}</div>")
        c = getEMLContentAsContext(file)
    elif file.type.startswith("text/"):
        t = Template("<div class='file-preview-content'>{{ content }}</div>")

        file.data.open(mode="rb")
        c = Context({'content': file.data.read()})
        file.data.close()

    return t.render(c)

def getEMLContentAsContext(file):
    data = email_util.getEmailData(file)

    ctx = {}
    ctx["subject"] = data["subject"] if data["subject"] is not None else ""
    ctx["from"] = "<" + data["from"] + ">" if data["from"] is not None else ""
    ctx["to"] = ", ".join(map(lambda x: "<" + x + ">", data["to"])) if data["to"] is not None else ""
    ctx["cc"] = ", ".join(map(lambda x: "<" + x + ">", data["cc"])) if data["cc"] is not None else ""
    ctx["bcc"] = ", ".join(map(lambda x: "<" + x + ">", data["bcc"])) if data["bcc"] is not None else ""
    ctx["datetime"] = data["datetime"] if data["datetime"] is not None else ""
    ctx["text"] = data["text"] if data["text"] is not None else ""

    return Context(ctx)
