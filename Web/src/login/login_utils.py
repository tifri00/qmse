from django.shortcuts import redirect

def login_required(request):
    return request.user is None or not request.user.is_authenticated

def redirect_login(request):
    return redirect("/login?redirect=%s" % request.build_absolute_uri())
