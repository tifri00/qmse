class SearchResult:
    def __init__(self, title, data, dataLines, sentenceString):
        self.title = title
        self.data = data
        self.sentenceString = sentenceString
        self.dataLines = dataLines

    def __str__(self):
        result = "Search result ('" + str(self.sentenceString) + "') [\n"
        result += "Title: '" + str(self.title) + "'\n"
        for header in self.data:
            result += header + ": " + str(self.data[header])
        result += "]"

        return result
