from django.db import models


# Create your models here.

class File(models.Model):
    title = models.CharField(max_length=64)
    description = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now=True)
    modified_at = models.DateTimeField(auto_now=True)
    data = models.FileField()
    type = models.CharField(max_length=32)
