from . import stanza_helper
from . import codecs
from .formulas import *
from . import ontology_utils
from . import constants
from . import query_builder
from . import prop_bank_helper
from . import query_evaluator
import subprocess
import itertools

class PredEvent:
    def __init__(self, v, rel, props):
        self.v = v
        self.rel = rel
        self.props = props

    def __str__(self):
        return "Event " + str(self.v) + ": \"" + str(self.rel) + "\""

def getOntologyTerms(term, user):
    if isinstance(term, Identifier):
        if term.i in constants.EVENT_IRIS.keys():
            return [Concept(constants.EVENT_IRIS[term.i])]

        if term.i in constants.MOD_IRIS.keys():
            return [Concept(constants.MOD_IRIS[term.i])]

        if term.i.lower() in ["i", "me", "my"]:
            term.i = user.username
        else:
            concepts = ontology_utils.findConceptsViaString(term.i)

            if concepts and len(concepts) > 0:
                return list(map(lambda x: Concept(x), concepts))

        concepts = ontology_utils.findConceptsViaLabel(term)

        if not concepts:
            concepts = []

        return [Identifier("\"" + term.i + "\"")] + concepts

    return [term]


def getOntologyTermsForRelation(term):
    if isinstance(term, Identifier):
        if term.i == constants.POSSESS_PLACEHOLDER:
            return [constants.HOLD_CONCEPT]

        if term.i in constants.EVENT_IRIS.keys():
            return [Concept(constants.EVENT_IRIS[term.i])]

        cevoEvent = ontology_utils.findCEVOEventViaLemma(term.i)
        if cevoEvent is not None:
            return [Concept(cevoEvent)]

    return [term]


def getOntologyFormulas(formula, sentence, user):
    result = []

    if isinstance(formula, Predicate):
        ps = getOntologyTerms(formula.p, user) if len(formula.args) == 1 else getOntologyTermsForRelation(formula.p)

        args = []
        for index, arg in enumerate(formula.args):
            if isinstance(arg, Identifier):
                if index == 1 and isinstance(formula.p, Identifier) and formula.p.i == constants.EVENT_REL_PLACEHOLDER:
                    args.append(getOntologyTermsForRelation(arg))

                elif isinstance(formula.p, Identifier) and formula.p.i in [constants.EVENT_NEG_PLACEHOLDER, "{count}", constants.EVENT_TMP_PLACEHOLDER]:
                    args.append([arg])

                else:
                    args.append(getOntologyTerms(arg, user))
            else:
                args.append([arg])

        argCombinations = list(itertools.product(*args))

        for p in ps:
            for argCombination in argCombinations:
                result.append(Predicate(p, list(argCombination)))

        return result
    elif isinstance(formula, Conjunction):
        f1s = getOntologyFormulas(formula.f1, sentence, user)
        f2s = getOntologyFormulas(formula.f2, sentence, user)

        for f1 in f1s:
            for f2 in f2s:
                result.append(Conjunction(f1, f2))

        return result
    elif isinstance(formula, Disjunction):
        f1s = getOntologyFormulas(formula.f1, sentence, user)
        f2s = getOntologyFormulas(formula.f2, sentence, user)

        for f1 in f1s:
            for f2 in f2s:
                result.append(Disjunction(f1, f2))

        return result
    elif isinstance(formula, Exists):
        fs = getOntologyFormulas(formula.f, sentence, user)

        for f in fs:
            result.append(Exists(formula.t, f))

        return result
    elif isinstance(formula, Negation):
        fs = getOntologyFormulas(formula.f, sentence, user)

        for f in fs:
            result.append(Negation(f))

        return result
    else:
        raise ValueError("The given object is not a formula.")


def identifyEventsHelper(formula, events):
    if isinstance(formula, Conjunction) or isinstance(formula, Disjunction):
        identifyEventsHelper(formula.f1, events)
        identifyEventsHelper(formula.f2, events)

    elif isinstance(formula, Negation):
        identifyEventsHelper(formula.f, events)

    elif isinstance(formula, Exists):
        identifyEventsHelper(formula.f, events)

    elif isinstance(formula, Predicate):
        if isinstance(formula.p, Identifier) and ((formula.p.i in constants.EVENT_IRIS) or formula.p.i == constants.EVENT_ARGM_PLACEHOLDER):
            corresponding_event = list(filter(lambda e: isinstance(formula.args[0], Variable) and formula.args[0].v == e.v.v, events))
            event = None
            if len(corresponding_event) == 0:
                event = PredEvent(formula.args[0], None, {})
                events.append(event)
            else:
                event = events[0]

            if len(formula.args) == 2 and isinstance(formula.args[1], Identifier) and formula.p.i == constants.EVENT_REL_PLACEHOLDER:
                event.rel = formula.args[1].i
            elif formula.p.i != constants.EVENT_REL_PLACEHOLDER:
                if formula.p.i not in event.props:
                    event.props[formula.p.i] = []

                event.props[formula.p.i].append(formula)


def identifyEvents(formula):
    events = []
    identifyEventsHelper(formula, events)

    result = []
    for event in events:
        if isinstance(event, PredEvent) and isinstance(event.v, Variable) and event.rel is not None and event.props is not None and len(event.props) >= 3:
            result.append(event)

    return result

def handleEvents(formula):
    events = identifyEvents(formula)
    
    for event in events:
        pag = event.props[constants.EVENT_PAG_PLACEHOLDER][0]
        ppt = event.props[constants.EVENT_PPT_PLACEHOLDER][0]
        if not isinstance(pag, Predicate) or not isinstance(ppt, Predicate):
            raise ValueError("Invalid values for agent and predicate")

        prop_bank_helper.determineArgTypes(formula, event.props, query_builder.isBound(pag.args[1], formula), query_builder.isBound(ppt.args[1], formula), event.rel)

    return events


def search(query, user):
    # Step 1: Parse using Stanza (includes preprocessing)
    (sentenceTrees, sentenceStrings) = stanza_helper.getSentenceTrees(query)

    for sentence in sentenceTrees:
        print(sentence)

    # Step 2: Transform sentences to formulas (parsing via montague and normalization)
    formulas = []
    for sentence in sentenceTrees:
        json = codecs.sentenceTreeToJSON(sentence)
        print("Sentence expressed as JSON: " + json)
        result = subprocess.run(["./" + constants.PARSER_PATH, json], capture_output=True, text=True).stdout
        print("Response of SML parser: " + result)
        formula = codecs.JSONToFormula(result)
        print("Sentence expressed as formula: " + str(formula))
        if formula is not None:
            formulas.append(formula)
        else:
            return []

    # Step 3: Handle event semantics placeholder (handling of ARGM attributes)
    events = []
    for formula in formulas:
        events.append(handleEvents(formula))

    #Step 4: ontology mapping
    ontologyFormulas = []
    for (formula, sentence) in zip(formulas, sentenceStrings):
        ontoForms = getOntologyFormulas(formula, sentence, user)
        ontologyFormulas.append(ontoForms)
        print("Ontology formulas:")
        print(list(map(lambda x: str(x), ontoForms)))

    # Step 5: Build query (body and select)
    queries = []
    for formulas in ontologyFormulas:
        qs = []
        for formula in formulas:
            (query, context) = query_builder.buildQuery(formula)
            print("Query:\n" + query)
            qs.append((query, context))

        queries.append(qs)

    # Step 6: for each sentence execute all queries; return a SearchResult object for each sentence that gives the most relevant result (evaluation and presentation)
    results = []
    for (qs, sentenceString, sentenceTree, qEvents, formulas) in zip(queries, sentenceStrings, sentenceTrees, events, ontologyFormulas):
        searchResult = query_evaluator.execAndEvaluate(qs, sentenceString, sentenceTree, qEvents, formulas)
        if searchResult is not None:
            results.append(searchResult)

    return results
