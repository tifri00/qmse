import django_tables2 as tables
from django.utils.safestring import mark_safe
from .formulas import *


class HTMLColumn(tables.Column):
    def render(self, value):
        return mark_safe(str(value)) if isinstance(value, str) else str(value)

class FileTable(tables.Table):
    title = tables.Column()
    description = tables.Column()
    created_at = tables.Column()
    modified_at = tables.Column()

    class Meta:
        attrs = {"class": "table-alternating-rowcols", "id": "main-file-table"}
        row_attrs = {"onClick": lambda record: "window.open('/file/details?file=" + str(record.pk) + "', '_self')"}

    def render_description(self, value):
        result = value[:50]

        if len(value) > 50:
            result += "..."

        return result


class DynamicColumnsTable(tables.Table):

    def __init__(self, *args, column_specs=(), **kwargs):
        bc = type(self.base_columns)(self.base_columns) # clone
        for name, column in column_specs:
            self.base_columns[name] = column
        tables.Table.__init__(self, *args, **kwargs) # refers to static spec
        type(self).base_columns = bc # restore static spec

    class Meta:
        attrs = {"class": "table-alternating-rowcols", "id": "main-file-table"}
