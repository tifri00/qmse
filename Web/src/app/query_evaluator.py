from . import SPARQL_utils as spql
from .formulas import *
from enum import Enum
from . import stanza_helper
from .SearchResult import SearchResult
from . import ontology_utils
from . import constants
from . import prop_bank_helper


class QuestionType(Enum):
    YES_NO = 1
    COUNT = 2
    SPECIFIC = 3


def removeDuplicates(data):
    if len(data) == 0:
        return 0

    initialLength = len(data[list(data.keys())[0]])

    dataMatrix = []
    for i in range(initialLength):
        line = []
        for col in data:
            line.append(data[col][i])
        dataMatrix.append(line)

    duplicateLines = []
    for i, line in enumerate(dataMatrix):
        if i in duplicateLines:
            continue

        for j, x in enumerate(dataMatrix):
            if j == i:
                continue
            elif x == line:
                duplicateLines.append(j)

    for i in reversed(duplicateLines):
        for col in data:
            del data[col][i]

    return initialLength - len(duplicateLines)


def findContextVarCounts(context):
    result = []
    for cd in context:
        if isinstance(cd, ContextVarCount):
            result.append(cd)

        res = findContextVarCounts(context[cd])
        if res is not None:
            result.extend(res)

    return result


def determineCount(data, context):
    cds = findContextVarCounts(context)

    if len(cds) == 1:
        return len(list(filter(lambda v: v is not None, data[cds[0].x.v[1:]])))
    else:
        pass  # TODO: handle arbitrary amount of counts

    return 0


# determines the type of a question
def determineQuestionType(context, sentenceTree):
    wrb = stanza_helper.find(sentenceTree, "WRB", None)

    if len(findContextVarCounts(context)) > 0 and wrb is not None:
        return QuestionType.COUNT
    elif wrb is not None or stanza_helper.find(sentenceTree, "WDT", ["det"]) is not None or stanza_helper.find(
            sentenceTree, "WP", ["nsubj", "nsubj:pass", "obj", "obl", "root"]) is not None:
        return QuestionType.SPECIFIC
    else:
        return QuestionType.YES_NO


# calculates whether a given query had the expected answer, i.e., if it is "true" or "false", which
# is particularly important for yes/no questions
def calculateTruthValue(data, vars, context):
    for cd in context:
        if not isinstance(cd, ContextTruth) and not isinstance(cd, ContextFalsity):
            if isinstance(cd, ContextCount):
                if len(list(filter(lambda v: v is not None, data[cd.x.v[1:]]))) != cd.n:
                    return False

            if not calculateTruthValue(data, vars, context[cd]):
                return False
        else:
            cdType = isinstance(cd, ContextTruth)

            values = data[cd.x.v[1:]]

            if cdType:
                if len(list(filter(lambda t: t is not None, values))) == 0 or not calculateTruthValue(data, vars,
                                                                                                      context[cd]):
                    return False
            else:
                if len(list(filter(lambda t: t is not None, values))) > 0:
                    return False

    return True


# determines the "title" of the response, i.e., a short answer based on the interpretation of the query response
def generateResponseTitle(truthValue, count, questionType):
    if questionType == QuestionType.YES_NO:
        return "Yes" if truthValue else "No"
    elif questionType == QuestionType.SPECIFIC:
        return "The following entities match your request:" if truthValue else "No entities matching your request were found"
    elif questionType == QuestionType.COUNT:
        return "There " + ("is " + str(count) + " entity" if count == 1 else "are " + str(
            count) + " entities") + " matching your request:"


# determines which variables of a query result should not be displayed in the final table
def determineVarsToRemove(data, events, context):
    result = []
    eventVars = list(map(lambda e: e.v.v[1:], events))

    for var in data:
        if len(list(filter(lambda v: v is not None, data[var]))) == 0:
            result.append(var)
            continue

        if var in eventVars:
            result.append(var)
            continue

    return result


# decides how values have to be displayed in the final table
# e.g., replaces properties by their {descr}
def handleTableValues(data):
    for var in data:
        for i, val in enumerate(data[var]):
            if isinstance(val, Concept):
                descr = ontology_utils.findDescrForConcept(val)
                if descr is not None:
                    data[var][i] = descr


def determineVariableNames(vars, data, formula, events, eventAttrMappings):
    result = {}

    if isinstance(formula, Conjunction) or isinstance(formula, Disjunction):
        f1Vars = determineVariableNames(vars, data, formula.f1, events, eventAttrMappings)
        result.update(f1Vars)
        f2Vars = determineVariableNames(list(filter(lambda v: v not in result.keys(), vars)), data, formula.f2, events,
                                        eventAttrMappings)
        result.update(f2Vars)

    elif isinstance(formula, Negation) or isinstance(formula, Exists):
        fVars = determineVariableNames(vars, data, formula.f, events, eventAttrMappings)
        result.update(fVars)

    elif isinstance(formula, Predicate):
        p = formula.p
        args = formula.args

        if len(args) == 1 and isinstance(args[0], Variable) and isinstance(p, Concept):
            if args[0].v[1:] in vars:
                label = ontology_utils.findConceptLabel(p)
                if label:
                    result[args[0].v[1:]] = label
        elif len(args) == 2 and isinstance(p, Concept) and isinstance(args[1], Variable) and args[1].v[1:] in vars:
            if len(list(filter(lambda v: not isinstance(v, Concept), data[args[1].v[1:]]))) == 0:
                commonType = ontology_utils.findCommonTypeForConcepts(list(set(data[args[1].v[1:]])))

                if commonType:
                    label = ontology_utils.findConceptLabel(Concept(commonType))
                    if label:
                        result[args[1].v[1:]] = label
                        return result

            if isinstance(args[0], Variable):
                event = next(filter(lambda e: e.v.v == args[0].v, events), None)

                if event:
                    try:
                        ev_index = list(constants.EVENT_IRIS.values()).index(p.iri)
                    except ValueError:
                        ev_index = None

                    ev_attr = list(constants.EVENT_IRIS.keys())[ev_index] if ev_index is not None else None

                    if ev_attr:
                        if ev_attr in eventAttrMappings[event.v.v]:
                            result[args[1].v[1:]] = eventAttrMappings[event.v.v][ev_attr]
                            return result

            if p.iri == constants.EVENT_IRIS[constants.EVENT_PAG_PLACEHOLDER]:
                result[args[1].v[1:]] = "Agent"
            elif p.iri == constants.EVENT_IRIS[constants.EVENT_PPT_PLACEHOLDER]:
                result[args[1].v[1:]] = "Patient"
            elif p.iri == constants.EVENT_IRIS[constants.EVENT_TMP_PLACEHOLDER]:
                result[args[1].v[1:]] = "Time"
            elif p.iri == constants.EVENT_IRIS[constants.EVENT_GOL_PLACEHOLDER]:
                result[args[1].v[1:]] = "Recipient"
            elif p.iri == constants.EVENT_IRIS[constants.EVENT_LOC_PLACEHOLDER]:
                result[args[1].v[1:]] = "Location"

    return result


# evaluates a given query result, i.e., prepares the SearchResult object by interpreting the result data
# as well as formatting it such that the user gets a meaningsful response
def evaluateResult(matrix, vars, context, sentenceString, sentenceTree, events, formula):
    print("Evaluate:")
    for res in matrix:
        for (val, var) in zip(res, vars):
            print(str(var) + ": " + str(val))
        print()

    print("Eval context:")
    print(context)

    # group the matrix by column
    data = {}
    for var, i in zip(vars, list(map(lambda v: vars.index(v), vars))):
        data[str(var)] = list(map(lambda l: l[i], matrix))

    # Step 1: calculate context truth value (i.e., according to the context hierarchy, does this result match?)
    # as well as the count (if the query expected something to be counted)
    truthValue = calculateTruthValue(data, vars, context)
    print("Truth value: " + str(truthValue))
    count = determineCount(data, context)

    # Step 2: determine question type (e.g., via key words), i.e., if the answer should be yes/no, a number
    # or simply a list of individuals (e.g., for what, who, where, ...)
    questionType = determineQuestionType(context, sentenceTree)
    print("Question type: " + str(questionType))

    # Step 3: depending on the question type, determine a "main response", i.e., the primary answer. E.g.,
    # if a user asked "Do all documents have a description?", the "main response" is "yes" and should accordingly
    # displayed as a huge kind of title over the result set
    responseTitle = generateResponseTitle(truthValue, count, questionType)
    print("Response title: " + str(responseTitle))

    if not truthValue:
        return SearchResult(responseTitle, {}, 0, sentenceString)

    # Step 4: determine which variables we want to output (depending on the truth value and the role of the
    # variable). E.g., vars without any results should generally not be displayed to the user. Also events
    # should normally not be displayed (only the agent, patient, etc.),
    # also take the context hierarchy into account, i.e., subordinates of a falsity ContextDecl should also not be displayed
    varsToRemove = determineVarsToRemove(data, events, context)
    for var in varsToRemove:
        data.pop(var, None)

    # convert all table entries to strings
    for var in data.keys():
        data[var] = list(map(lambda v: v, data[var]))

    # Step 5: compute proper variable names (e.g., according to the type of the references entities)
    keys = list(data.keys())
    eventAttrMappings = {}
    for event in events:
        eventAttrMappings[event.v.v] = prop_bank_helper.findRoleNamesByLemma(event.rel)
    new_vars = determineVariableNames(keys, data, formula, events, eventAttrMappings)
    for old_var in new_vars:
        data[new_vars[old_var]] = data.pop(old_var)

    # Step 6: post process
    # Compute additional columns or replace columns (e.g., replace iris by their {descr})
    handleTableValues(data)

    # Step 7: remove duplicates
    resultLength = removeDuplicates(data)

    return SearchResult(responseTitle, data, resultLength, sentenceString)


def determineEventDomRngMap(formula, eventVars):
    result = {}

    if isinstance(formula, Conjunction) or isinstance(formula, Disjunction):
        f1Res = determineEventDomRngMap(formula.f1, eventVars)
        f2Res = determineEventDomRngMap(formula.f2, eventVars)

        for ev in f1Res:
            if not ev in result:
                result[ev] = f1Res[ev]
            else:
                if result[ev][0] is None:
                    result[ev][0] = f1Res[ev][0]
                if result[ev][1] is None:
                    result[ev][1] = f1Res[ev][1]

        for ev in f2Res:
            if not ev in result:
                result[ev] = f2Res[ev]
            else:
                if result[ev][0] is None:
                    result[ev][0] = f2Res[ev][0]
                if result[ev][1] is None:
                    result[ev][1] = f2Res[ev][1]

    elif isinstance(formula, Negation) or isinstance(formula, Exists):
        fRes = determineEventDomRngMap(formula.f, eventVars)

        for ev in fRes:
            if not ev in result:
                result[ev] = fRes[ev]
            else:
                if result[ev][0] is None:
                    result[ev][0] = fRes[ev][0]
                if result[ev][1] is None:
                    result[ev][1] = fRes[ev][1]

    elif isinstance(formula, Predicate):
        args = formula.args
        p = formula.p

        if len(args) == 2 and isinstance(args[0], Variable) and args[0] in eventVars and isinstance(p, Concept) and (
                p.iri in [constants.EVENT_IRIS[constants.EVENT_PAG_PLACEHOLDER],
                          constants.EVENT_IRIS[constants.EVENT_PPT_PLACEHOLDER]]):
            if args[0] not in result:
                result[args[0]] = [None, None]

            if p.iri == constants.EVENT_IRIS[constants.EVENT_PAG_PLACEHOLDER]:
                result[args[0]][0] = args[1]
            else:
                result[args[0]][1] = args[1]

    return result


def findConceptAssertionForVariable(formula, var):
    if isinstance(formula, Conjunction) or isinstance(formula, Disjunction):
        res = findConceptAssertionForVariable(formula.f1, var)

        if not res:
            return findConceptAssertionForVariable(formula.f2, var)

        return res

    elif isinstance(formula, Negation) or isinstance(formula, Exists):
        return findConceptAssertionForVariable(formula.f, var)

    elif isinstance(formula, Predicate):
        p = formula.p
        args = formula.args

        if len(args) == 1 and args[0] == var and isinstance(p, Concept):
            return formula

    return None


# gets a agent/patient value (string, concept or variable) and finds the corresponding type
# for a string, the single type "literal" is returned
# for a concept, a list of the concept's types or, if no types were found, the concept itself is returned
# for a variable for which a concept assertion exists, the corresponding concept is returned
# otherwise, for each value in the column, a list of the value's types is returned (or None if the value is None or "?" if the type
# cannot be determined)
def determineColumnType(col, data, formula):
    if isinstance(col, Identifier):
        return [["literal"]]
    elif isinstance(col, Concept):
        types = ontology_utils.getTypesOfConcept(col)
        return [types] if len(types) > 0 else [[col.iri]]
    elif isinstance(col, Variable):
        concAssert = findConceptAssertionForVariable(formula, col)

        if concAssert:
            return [[concAssert.p.iri]]

        if str(col)[1:] in data:
            vals = data[str(col)[1:]]
            typeList = []
            for val in vals:
                if val is None:
                    typeList.append(None)
                    continue

                types = ontology_utils.getTypesOfConcept(Concept(val))
                if len(types) == 0:
                    types = ["?"]
                typeList.append(types)

            if len(typeList) > 0:
                return typeList

    return [["?"]]


# gets a dom and rng value that is either a string, a concept or a variable and builds a map of types of the corresponding event
# this results in a list of lists of tuples where each of the lists determines one set of types that should be satisified by the event
# i.e., the event should have at least ONE of the types per list, otherwise there are type mismatches
def buildEventTypeMap(dom, rng, data, formula):
    if dom is None or rng is None:
        return []

    domTypes = determineColumnType(dom, data, formula)
    domLen = len(domTypes)
    rngTypes = determineColumnType(rng, data, formula)
    rngLen = len(rngTypes)

    if domLen == 0 or rngLen == 0:
        return []

    result = []

    for i in range(max(domLen, rngLen)):
        doms = domTypes[0] if i >= domLen else domTypes[i]
        rngs = rngTypes[0] if i >= rngLen else rngTypes[i]

        res = []

        for dom in doms:
            for rng in rngs:
                if dom is None or rng is None:
                    continue

                if (dom, rng) not in result:
                    res.append((dom, rng))

        if not res in result:
            result.append(res)

    return result


def determineActualTypeMapForEventRel(rel):
    domRngPairs = ontology_utils.getDomainRangePairsByCEVOVerb(rel)

    result = []

    for domRngPair in domRngPairs:
        dom = "literal" if domRngPair[0] is None else domRngPair[0]
        rng = "literal" if domRngPair[1] is None else domRngPair[1]

        if not (dom, rng) in result:
            result.append((dom, rng))

    return result


def getCEVOVerbByEventVarAndFormula(var, formula):
    if isinstance(formula, Conjunction) or isinstance(formula, Disjunction):
        res = getCEVOVerbByEventVarAndFormula(var, formula.f1)
        if res is None:
            res = getCEVOVerbByEventVarAndFormula(var, formula.f2)

        return res

    elif isinstance(formula, Negation) or isinstance(formula, Exists):
        return getCEVOVerbByEventVarAndFormula(var, formula.f)

    elif isinstance(formula, Predicate) and len(formula.args) == 2 and isinstance(formula.p, Concept) and formula.p.iri == \
            constants.EVENT_IRIS[constants.EVENT_REL_PLACEHOLDER] and formula.args[0] == var and isinstance(formula.args[1], Concept):
        return formula.args[1]

    return None


def containedInTypeMap(t, typeMap):
    for tp in typeMap:
        if (t[0] == "?" or t[0] == tp[0]) and (t[1] == "?" or t[1] == tp[1]):
            return True

    return False


# assigns a score to a query result such that it can be determined which result to use for the final answer
# the score lies between -1 and 1 and is related to the percentage of types in the result that match the actual event's types
# if no types match, the score is 0 and if every type matches, the score is 1
# if malformed events exist, i.e., events without a pag or ppt (which should never occur), a score of -1 is returned
def calculateScore(result, formula, events, eventTypes):
    score = 0

    matrix = spql.getMatrix(result)
    vars = spql.getVars(result)

    data = {}
    for var, i in zip(vars, list(map(lambda v: vars.index(v), vars))):
        data[str(var)] = list(map(lambda l: l[i], matrix))

    # contains a length 2 list of the form [agent, patient] for every event
    # i.e., agent/patient is either a string, a concept or a variable
    eventDomRngMap = determineEventDomRngMap(formula, list(map(lambda e: e.v, events)))

    if not eventDomRngMap or len(eventDomRngMap) == 0:
        return 0

    for ev in eventDomRngMap:
        typeMap = buildEventTypeMap(eventDomRngMap[ev][0], eventDomRngMap[ev][1], data, formula)
        verb = getCEVOVerbByEventVarAndFormula(ev, formula)
        if verb not in eventTypes:
            actualTypeMap = determineActualTypeMapForEventRel(verb)
            eventTypes[verb] = actualTypeMap
        else:
            actualTypeMap = eventTypes[verb]

        if len(typeMap) == 0:
            return -1

        for typeSet in typeMap:
            if len(list(filter(lambda t: containedInTypeMap(t, actualTypeMap), typeSet))) > 0:
                score += 1 / len(typeMap)

    return score / len(eventDomRngMap)


# finds the "best" query result
def findBestResult(results, events):
    bestResult = None
    bestScore = None
    eventTypes = {}
    for (result, context, formula) in results:
        score = calculateScore(result, formula, events, eventTypes)
        if bestScore is None or score > bestScore:
            bestScore = score
            bestResult = (result, context, formula)

    return bestResult


# executes a given set of queries and returns a SearchResult for the best result
def execAndEvaluate(queries, sentenceString, sentenceTree, events, formulas):
    queryResults = []

    for ((query, context), formula) in zip(queries, formulas):
        result = spql.execQuery(query)
        if result is not None:
            queryResults.append((result, context, formula))

    bestResult = findBestResult(queryResults, events)

    if bestResult is None:
        return None

    print("Best result: " + str(bestResult[2]))

    matrix = spql.getMatrix(bestResult[0], True)
    vars = spql.getVars(bestResult[0])
    context = bestResult[1]
    searchResult = evaluateResult(matrix, vars, context, sentenceString, sentenceTree, events, bestResult[2])

    return searchResult
