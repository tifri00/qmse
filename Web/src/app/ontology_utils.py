from . import SPARQL_utils as spql
from nltk.corpus import wordnet as wn
from . import constants
import json
from Levenshtein import distance as lev_distance
from .formulas import *

wn_mappings = {}

def init():
    global wn_mappings
    with open(constants.WN_MAPPINGS_PATH) as fp:
        data = json.load(fp)
        for map_data in data:
            if map_data["wordnet_version_from"] == 3.1 and map_data["wordnet_version_to"] == 3.0:
                wn_mappings = map_data["synset-mapping"]
                break


def findSynonymsForVerbLemmaViaWordnet(lemma):
    wn_result = wn.synsets(lemma, pos=wn.VERB)

    lemmas = []
    for wn_res in wn_result:
        lemmas.extend(wn_res.lemma_names())

    lemmas = list(set(lemmas))
    if lemma in lemmas:
        lemmas.remove(lemma)

    return lemmas


def formatWordnetId(id):
    formattedId = str(id)
    if len(formattedId) < 8:
        formattedId = (8 - len(formattedId)) * "0" + formattedId
    formattedId = "1" + formattedId

    return formattedId


def wn31Town30(id):
    global wn_mappings
    id = id[1:]

    while id.startswith("0"):
        id = id[1:]

    id = "n" + id

    if not id in wn_mappings:
        return None

    result = wn_mappings[id]

    return formatWordnetId(result[1:])


def findConceptViaSynsetId(id):
    result = spql.execQuery("""SELECT ?c WHERE {
            ?c yago:hasSynsetId \"""" + str(id) + """\".
        }""")
    matrix = spql.getMatrix(result)
    if matrix is None or len(matrix) == 0 or len(matrix[0]) == 0:
        return None

    return matrix[0][0]


def findCEVOEventViaLemmaInternal(lemma):
    result = spql.execQuery("""SELECT DISTINCT ?event ?evLabel WHERE {
    	        ?v rdf:type cevo:Verb.
      	        ?v rdf:type ?event.
    	        ?v rdfs:label ?vlabel.
    	        ?event rdfs:label ?evLabel.
    	        Filter (STR(?vlabel) = \"""" + str(lemma) + """\").
    	        Filter (?event != cevo:Verb).
    	        Filter (?event != owl:NamedIndividual).
            }""")
    matrix = spql.getMatrix(result)

    if matrix is None or len(matrix) == 0 or len(matrix[0]) == 0:
        return (None, None)

    if len(matrix) == 1:
        return (str(matrix[0][0]), lev_distance(str(matrix[0][0]), str(lemma)))

    result = None
    bestScore = None
    for res in matrix:
        score = lev_distance(str(res[1]), str(lemma))
        if bestScore is None or score < bestScore:
            bestScore = score
            result = str(res[0])

    return (result, bestScore)


def findCEVOEventViaLemma(lemma):
    (result, d) = findCEVOEventViaLemmaInternal(lemma)

    if result:
        return result

    synonyms = findSynonymsForVerbLemmaViaWordnet(lemma)

    finalResult = None
    bestScore = None
    for synonym in synonyms:
        (result, d) = findCEVOEventViaLemmaInternal(synonym)

        if result is None:
            continue

        if bestScore is None or d < bestScore or (d == bestScore and lev_distance(result, lemma) < lev_distance(finalResult, lemma)):
            finalResult = result
            bestScore = d

    return finalResult


def findConceptsViaString(term):
    # Find corresponding wordnet synsets
    synsetIds = list(map(lambda s: formatWordnetId(s.offset()), wn.synsets(term, pos=wn.NOUN)))

    if len(synsetIds) == 0:
        return []

    result = []
    for synsetId in synsetIds:
        concept = findConceptViaSynsetId(synsetId)
        if concept is not None:
            result.append(concept)

    return result if len(result) > 0 else [term]



def deleteTriplesRelatedToConcept(concept):
    query = """DELETE WHERE {
                ?e a <""" + constants.EVENT_IRIS["{event}"] + """>.
                ?e ?ep """ + str(concept) + """.
                ?e ?ep2 ?eo.
            }"""

    spql.execUpdate(query)

    query = """DELETE WHERE {
                    """ + str(concept) + """ ?p ?o.
                }"""

    spql.execUpdate(query)

    query = """DELETE WHERE {
                        ?s """ + str(concept) + """ ?o.
                    }"""

    spql.execUpdate(query)

    query = """DELETE WHERE {
                        ?s ?p """ + str(concept) + """.
                    }"""

    spql.execUpdate(query)


def findDescrForConcept(concept):
    if not isinstance(concept, Concept):
        return None

    query = """SELECT ?d WHERE {
                """ + str(concept) + """ <""" + constants.DESCR_IRI + """> ?d
            }"""

    result = spql.execQuery(query)

    matrix = spql.getMatrix(result)
    if matrix is None or len(matrix) == 0 or len(matrix[0]) == 0:
        return None

    return matrix[0][0]


def findConceptsViaLabel(label):
    query = """SELECT ?c WHERE {
                    ?c """ + constants.LABELLING_RELATIONS + """ \"""" + str(label) + """\"
                }"""

    result = spql.execQuery(query)

    matrix = spql.getMatrix(result)
    if matrix is None or len(matrix) == 0 or len(matrix[0]) == 0:
        return None

    labelledEntities = list(map(lambda l: Concept(l[0]), matrix))
    result = labelledEntities

    for le in labelledEntities:
        concs = findConceptsHavingProperty(le)
        if concs:
            result += concs

    return list(set(result))


def findConceptsHavingProperty(property):
    query = """SELECT ?c WHERE {
                        ?e a <""" + str(constants.EVENT_IRIS["{event}"]) + """>.
                        ?e <""" + str(constants.EVENT_IRIS[constants.EVENT_PAG_PLACEHOLDER]) + """> ?c.
                        ?e <""" + str(constants.EVENT_IRIS[constants.EVENT_REL_PLACEHOLDER]) + """> """ + str(constants.HOLD_CONCEPT) + """.
                        ?e <""" + str(constants.EVENT_IRIS[constants.EVENT_PPT_PLACEHOLDER]) + """> """ + str(property) + """.
                    }"""

    result = spql.execQuery(query)

    matrix = spql.getMatrix(result)
    if matrix is None or len(matrix) == 0 or len(matrix[0]) == 0:
        return None

    return list(map(lambda l: Concept(l[0]), matrix))


def findConceptLabel(conc):
    query = """SELECT ?l WHERE {
                        """ + str(conc) + """ skos:prefLabel ?l
                    }"""

    result = spql.execQuery(query)

    matrix = spql.getMatrix(result)
    if matrix is None or len(matrix) == 0 or len(matrix[0]) == 0:
        return None

    return matrix[0][0]


def findCommonTypeForConcepts(concs):
    query = """SELECT ?t WHERE {
                            """ + "\n".join(list(map(lambda c: str(c) + " a ?t.", concs))) + """
                        }"""

    result = spql.execQuery(query)

    matrix = spql.getMatrix(result)
    if matrix is None or len(matrix) == 0 or len(matrix[0]) == 0:
        return None

    return matrix[0][0]


def getDomainRangePairsByCEVOVerb(verb):
    if not isinstance(verb, Concept):
        return []

    query = """SELECT ?domt ?rngt WHERE {
                    ?e <""" + constants.EVENT_IRIS[constants.EVENT_PAG_PLACEHOLDER] + """> ?dom.
                    OPTIONAL {
                        ?dom a ?domt.
                    }
                    ?e <""" + constants.EVENT_IRIS[constants.EVENT_REL_PLACEHOLDER] + """> """ + str(verb) + """.
                    ?e <""" + constants.EVENT_IRIS[constants.EVENT_PPT_PLACEHOLDER] + """> ?rng.
                    OPTIONAL {
                        ?rng a ?rngt.
                    }
                }"""

    result = spql.execQuery(query)

    matrix = spql.getMatrix(result)
    if matrix is None:
        return []

    return matrix

def getTypesOfConcept(conc):
    if not isinstance(conc, Concept):
        return []

    query = """SELECT ?t WHERE {
                    """ + str(conc) + """ a ?t.
                }"""

    result = spql.execQuery(query)

    matrix = spql.getMatrix(result)
    if matrix is None or len(matrix) == 0 or len(matrix[0]) == 0:
        return []

    return list(map(lambda l: l[0], matrix))
