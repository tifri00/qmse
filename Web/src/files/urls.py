from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('details', views.details),
    path('edit', views.edit),
    path('download', views.download),
    path('delete', views.delete),
]
