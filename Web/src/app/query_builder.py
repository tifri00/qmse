from .formulas import *
from . import constants
from datetime import datetime, timedelta

var_counter = 0

def freshVar():
    global var_counter
    var_counter += 1

    return "?y" + str(var_counter)

def isExists(formula, ignoreNegation=True):
    return isinstance(formula, Exists) or\
           (ignoreNegation and isinstance(formula, Negation) and isinstance(formula.f, Exists))


def getExistsIgnoringNegation(formula):
    if isinstance(formula, Exists):
        return formula

    if isinstance(formula, Negation) and isinstance(formula.f, Exists):
        return formula.f

    return None


def deleteFromContext(context, x):
    removeList = []
    for ctx in context:
        if isinstance(ctx, ContextDecl) and (ctx.x.v == x.v or ctx.x.v == x.v):
            removeList.append(ctx)

    for rem in removeList:
        context.remove(rem)


def computeDateTimeRangeForString(dts):
    dateFrom = ""
    dateTo = ""
    if dts == "yesterday":
        today = datetime.today()
        yesterday = today - timedelta(days=1)
        dateFrom = "\"" + yesterday.strftime("%Y-%m-%dT00:00:01") + "\"^^xsd:dateTime"
        dateTo = "\"" + yesterday.strftime("%Y-%m-%dT23:59:59") + "\"^^xsd:dateTime"
    elif dts == "today":
        today = datetime.today()
        dateFrom = "\"" + today.strftime("%Y-%m-%dT00:00:01") + "\"^^xsd:dateTime"
        dateTo = "\"" + today.strftime("%Y-%m-%dT23:59:59") + "\"^^xsd:dateTime"
    elif dts == "tomorrow":
        today = datetime.today()
        tomorrow = today + timedelta(days=1)
        dateFrom = "\"" + tomorrow.strftime("%Y-%m-%dT00:00:01") + "\"^^xsd:dateTime"
        dateTo = "\"" + tomorrow.strftime("%Y-%m-%dT23:59:59") + "\"^^xsd:dateTime"
    else:
        datePatterns = ["%d-%m-%Y", "%d.%m.%Y", "%Y-%m-%d", "%Y/%m/%d", "%d/%m/%Y"]

        date = None
        for pattern in datePatterns:
            try:
                date = datetime.strptime(dts, pattern).date()
            except:
                pass

            if date is not None:
                break

        if date is not None:
            dateFrom = "\"" + date.strftime("%Y-%m-%dT00:00:01") + "\"^^xsd:dateTime"
            dateTo = "\"" + date.strftime("%Y-%m-%dT23:59:59") + "\"^^xsd:dateTime"

    return (dateFrom, dateTo)


def buildPredicate(formula, negated, context):
    p = formula.p
    args = formula.args

    if len(args) == 1:
        if negated:
            return str(args[0]) + " a " + str(freshVar()) + ".\nFILTER NOT EXISTS {\n" + buildPredicate(formula, False, context) + "}\n"
        else:
            return str(args[0]) + " a " + str(p) + ".\n"
    elif len(args) == 2:
        if negated:
            return "FILTER NOT EXISTS {\n" + buildPredicate(formula, False, context) + "}\n"
        else:
            if isinstance(p, Identifier):
                if p.i == "{count}":
                    t = formula.args[0]
                    n = formula.args[1]

                    if not isinstance(t, Variable):
                        return ""

                    deleteFromContext(context, t)
                    if isinstance(n, Variable):
                        deleteFromContext(context, n)
                        context.append(ContextVarCount(t))
                    elif isinstance(n, Identifier):
                        context.append(ContextCount(t, int(n.i)))

                    return ""

            elif isinstance(p, Concept) and isinstance(args[1], Identifier) and p.iri == constants.EVENT_IRIS[constants.EVENT_TMP_PLACEHOLDER]:
                (dateFrom, dateTo) = computeDateTimeRangeForString(str(args[1]).replace("\"", "").replace("'", ""))
                dateVar = freshVar()
                res = str(args[0]) + " " + str(p) + " " + str(dateVar) + ".\n"
                res += "FILTER(" + str(dateVar) + " >= " + str(dateFrom) + " && " + str(dateVar) + " <= " + str(dateTo) + ")\n"
                return res

            return str(args[0]) + " " + str(p) + " " + str(args[1]) + ".\n"
    else:
        raise ValueError("The formula is invalid as it contains a predicate with " + str(len(args)) + " arguments.")


def buildBody(formula, context):
    if isinstance(formula, Predicate):
        res = buildPredicate(formula, False, context)
        return res

    elif isinstance(formula, Conjunction):
        f1 = formula.f1
        f2 = formula.f2

        f1IsExists = isExists(f1)
        f2IsExists = isExists(f2)
        f1IsNegation = isinstance(f1, Negation)
        f2IsNegation = isinstance(f2, Negation)
        if len(context) == 0 and ((f1IsExists and f1IsNegation) or (f2IsExists and f2IsNegation)):
            isF1 = f1IsExists and f1IsNegation
            ex = getExistsIgnoringNegation(f1 if isF1 else f2)

            context.append(ContextFalsity(ex.t))

            res1 = buildBody(ex.f if isF1 else f1, context)
            res2 = buildBody(f2 if isF1 else ex.f, context)

            return "{\n" + res1 + "} UNION {\n" + res2 + "}.\n"

        res1 = buildBody(f1, context)
        res2 = buildBody(f2, context)

        return res1 + res2

    elif isinstance(formula, Disjunction):
        res1 = buildBody(formula.f1, context)
        res2 = buildBody(formula.f2, context)

        return "{\n" + res1 + "} UNION {\n" + res2 + "}.\n"

    elif isinstance(formula, Exists):
        context.append(ContextTruth(formula.t))

        return buildBody(formula.f, context)


    elif isinstance(formula, Negation):
        if isinstance(formula.f, Predicate):
            return buildPredicate(formula.f, True, context)

        elif isinstance(formula.f, Exists):
            contextNotEmpty = len(context) > 0

            context.append(ContextFalsity(formula.f.t))

            if contextNotEmpty:
                return "FILTER NOT EXISTS {\n" + buildBody(formula.f.f, context) + "}\n"

            else:
                return buildBody(formula.f.f, context)

    return ""


def buildSelect(context):
    selects = []
    for ctxDecl in context:
        selects.append(str(ctxDecl.x))

    if len(selects) == 0:
        selects.append("*")

    return "SELECT DISTINCT " + " ".join(selects) + " WHERE"


def isBound(variable, formula):
    if not isinstance(variable, Variable):
        return True

    if isinstance(formula, Predicate):
        return False

    elif isinstance(formula, Conjunction) or isinstance(formula, Disjunction):
        res1 = isBound(variable, formula.f1)
        res2 = isBound(variable, formula.f2)

        return res1 or res2

    elif isinstance(formula, Exists):
        if isinstance(formula.t, Variable) and formula.t.v == variable.v:
            return True

        return isBound(variable, formula.f)

    elif isinstance(formula, Negation):
        return isBound(variable, formula.f)


def mergeContextHierarchies(ctx1, ctx2):
    result = ctx1

    for cd in ctx2:
        if cd in result:
            result[cd] = mergeContextHierarchies(result[cd], ctx2[cd])
        else:
            result[cd] = ctx2[cd]

    return result


def computeContextHierarchy(context, formula):
    result = {}
    # compute context hierarchy by traversing the formula and sorting the context declarations accordingly

    if isinstance(formula, Variable):
        return result

    if isinstance(formula, Predicate):
        return result

    elif isinstance(formula, Conjunction) or isinstance(formula, Disjunction):
        res1 = computeContextHierarchy(context, formula.f1)
        res2 = computeContextHierarchy(context, formula.f2)

        return mergeContextHierarchies(res1, res2)

    elif isinstance(formula, Exists):
        fRes = computeContextHierarchy(context, formula.f)

        cd = next(filter(lambda cd: isinstance(cd, ContextDecl) and cd.x.v == formula.t.v, context), None)
        if isinstance(formula.t, Variable) and cd is not None:
            result[cd] = fRes
        else:
            result = fRes

        return result

    elif isinstance(formula, Negation):
        return computeContextHierarchy(context, formula.f)

    return context


def buildQuery(formula):
    context = []
    body = buildBody(formula, context)
    context = list(set(context))
    print("Context: " + str(list(map(lambda x: str(x), context))))
    select = buildSelect(context)

    contextHierarchy = computeContextHierarchy(context, formula)

    return select + " {\n" + body + "}", contextHierarchy
