from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from app import information_extractor

# Create your views here.

def login_page(request):
    if request.user.is_authenticated:
        return redirect("/")

    context = {
        'invalid_login': False,
    }

    if request.method == 'POST':
        username = request.POST.get("username")
        password = request.POST.get("password")
        redirectURL = request.POST.get("redirect")

        user = authenticate(request, username=username, password=password)

        if user is None:
            context['invalid_login'] = True
            context['redirect'] = redirectURL
            return render(request, 'login.html', context)

        login(request, user)

        information_extractor.extractUserInfo(request.user)

        if redirectURL:
            return redirect(redirectURL)
        else:
            return redirect("/")
    elif request.method == 'GET':
        context['redirect'] = request.GET.get('redirect')

    return render(request, 'login.html', context)

def logout_page(request):
    logout(request)

    return redirect("/login")