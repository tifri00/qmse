import datetime
import os

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotAllowed, Http404
from django.core.exceptions import ObjectDoesNotExist

from django.conf import settings
from .models import File
from login import login_utils
from . import FileRenderUtil as fru
from app import information_extractor
from app.models import ExtractedFileInfo
from app import ontology_utils
from app.formulas import *


# Create your views here.

def details(request):
    if login_utils.login_required(request):
        return login_utils.redirect_login(request)

    if not request.method == "GET":
        return HttpResponseNotAllowed(["GET"])

    fileId = request.GET.get("file")

    if not fileId:
        return HttpResponseBadRequest("Missing required GET parameter 'file'")

    try:
        file = File.objects.get(id=fileId)
    except ObjectDoesNotExist:
        return HttpResponseBadRequest("The requested file does not exist")

    fileRendering = fru.getFileRendering(file)

    return render(request, 'details.html', {'file': file, 'fileRendering': fileRendering})

def edit(request):
    if login_utils.login_required(request):
        return login_utils.redirect_login(request)

    if request.method == "POST":
        fileId = request.POST.get("id")
        title = request.POST.get("title")
        description = request.POST.get("description")
        data = request.FILES.get("data")

        file = None

        if fileId:
            try:
                file = File.objects.get(id=fileId)
            except ObjectDoesNotExist:
                pass
        else:
            fileId = -1

        errors = []
        if not title:
            errors.append("Please enter a title")
        if not data and not file:
            errors.append("Please select a file")

        if len(errors) > 0:
            file = File(title=title, description=description)
            return render(request, 'edit.html', {"errors": errors, "file": file, "fileId": fileId})

        if not file:
            file = File(title=title, description=description, created_at=datetime.date.today())
        else:
            file.title = title
            file.description=description

        if data:
            if file.data:
                os.remove(os.path.join(settings.MEDIA_ROOT, file.data.name))

            file.data = data
            file.type = data.content_type

        file.modified_at = datetime.date.today()

        file.save()

        information_extractor.extract(file, request.user)


        return redirect('/file/details?file=%s' % file.id)

    fileId = request.GET.get("file")

    context = {
        "file": None,
        "fileId": -1,
    }

    if fileId:
        try:
            context["file"] = File.objects.get(id=fileId)
            context["fileId"] = int(fileId)
        except ObjectDoesNotExist:
            return HttpResponseBadRequest("The requested file does not exist")

    return render(request, 'edit.html', context)

def download(request):
    if request.method == "GET":
        fileId = request.GET.get("file")

        if not fileId:
            return HttpResponseBadRequest("Missing required query parameter file")

        file = None
        try:
            file = File.objects.get(id=fileId)
        except ObjectDoesNotExist:
            return Http404("File not found")

        file_path = os.path.join(settings.MEDIA_ROOT, file.data.name)
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type=file.type)
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                return response

        return Http404("File not found")

    return HttpResponseNotAllowed(['GET'])

def delete(request):
    if request.method != "POST":
        return redirect("/")

    fileId = request.POST.get("fileId")

    extractionConcepts = ExtractedFileInfo.objects.filter(file__id=fileId)

    for extractionConcept in extractionConcepts:
        ontology_utils.deleteTriplesRelatedToConcept(Concept(extractionConcept.concept))
        extractionConcept.delete()

    file = File.objects.get(id=fileId)
    file.data.delete()
    file.delete()

    return redirect("/")
