from . import email_util
import time
from . import constants
from .formulas import *
from datetime import datetime
from . import SPARQL_utils as spql
from .models import ExtractedUserInfo
from .models import ExtractedFileInfo


def individual(name):
    return Concept(constants.QMSE_DATA_IRI + "#" + name)


def conceptAssertion(individual, concept):
    return (individual, "rdf:type", concept)


def dateTime(datetimeObj):
    return "\"" + str(datetimeObj.isoformat()) + "\"^^xsd:dateTime"


def event(name, pag, rel, ppt, attrs, negated=False):
    result = []

    ev = individual("event_" + str(name))
    result.append(conceptAssertion(ev, Concept(constants.EVENT_IRIS["{event}"])))

    if pag is not None:
        result.append((ev, Concept(constants.EVENT_IRIS[constants.EVENT_PAG_PLACEHOLDER]), pag))

    result.append((ev, Concept(constants.EVENT_IRIS[constants.EVENT_REL_PLACEHOLDER]), rel))

    if ppt is not None:
        result.append((ev, Concept(constants.EVENT_IRIS[constants.EVENT_PPT_PLACEHOLDER]), ppt))

    for attr in attrs:
        result.append((ev, Concept(constants.EVENT_IRIS[attr[0]]), attr[1]))

    result.append((ev, Concept(constants.EVENT_IRIS[constants.EVENT_NEG_PLACEHOLDER]), "true" if negated else "false"))

    return result


def rdfString(text):
    return str("\"" + text.replace("\n", "\\n").replace("\r", "\\r").replace('"', '\\"') + "\"")


def extract(file, user):
    extracted_triples = []

    extractedEntities = []

    timestamp = time.time_ns()
    ID = "document_" + str(timestamp)
    fileIndividual = individual(ID)
    extractedEntities.append(ExtractedFileInfo(file=file, concept=fileIndividual.iri))

    creationDateTime = dateTime(datetime.now())

    # create a new file instance
    extracted_triples.append(conceptAssertion(fileIndividual, constants.DOCUMENT_CONCEPT))

    # add description (link to file)
    extracted_triples.append((fileIndividual, Concept(constants.DESCR_IRI), rdfString("<a href='/file/details?file=" + str(file.id) + "'>" + str(file.title) + "</a>")))

    extractedUserInfo = ExtractedUserInfo.objects.filter(user=user, isUserIndividual=True).first()

    # add creation event (user create file + time)
    extracted_triples.extend(event(ID + "_create",
                                   Concept(extractedUserInfo.concept),
                                   constants.CREATE_CONCEPT,
                                   fileIndividual,
                                   [
                                       (constants.EVENT_TMP_PLACEHOLDER, creationDateTime)
                                   ]))

    if creationDateTime is not None:
        creationDateTimeIndividual = individual(ID + "_creationDatetime")
        extractedEntities.append(ExtractedFileInfo(file=file, concept=creationDateTimeIndividual.iri))
        extracted_triples.append(conceptAssertion(creationDateTimeIndividual, constants.DOCUMENT_CREATION_DATETIME_CONCEPT))
        extracted_triples.extend(event(ID + "_creationDatetime",
                                       fileIndividual,
                                       constants.HOLD_CONCEPT,
                                       creationDateTimeIndividual,
                                       []))
        extracted_triples.append((creationDateTimeIndividual, Concept(constants.DESCR_IRI), creationDateTime))


    titleIndividual = individual(ID + "_title")
    extractedEntities.append(ExtractedFileInfo(file=file, concept=titleIndividual.iri))
    extracted_triples.append(conceptAssertion(titleIndividual, constants.TITLE_CONCEPT))
    extracted_triples.extend(event(ID + "_title",
                                   fileIndividual,
                                   constants.HOLD_CONCEPT,
                                   titleIndividual,
                                   []))
    extracted_triples.append((titleIndividual, Concept(constants.DESCR_IRI), rdfString(file.title)))

    descrIndividual = individual(ID + "_descr")
    extractedEntities.append(ExtractedFileInfo(file=file, concept=descrIndividual.iri))
    extracted_triples.append(conceptAssertion(descrIndividual, constants.DESCRIPTION_CONCEPT))
    extracted_triples.extend(event(ID + "_descr",
                                   fileIndividual,
                                   constants.HOLD_CONCEPT,
                                   descrIndividual,
                                   []))
    extracted_triples.append((descrIndividual, Concept(constants.DESCR_IRI), rdfString(file.description)))

    spql.insertTriples(extracted_triples)
    print("Extracted " + str(len(extracted_triples)) + " triples")

    for extractedEntity in extractedEntities:
        extractedEntity.save()

    if file.type == "application/pdf":
        print("Extracting PDF content...")
    elif file.type == "message/rfc822":
        extract_eml(file)
    elif file.type.startswith("text/"):
        print("Extracting TXT content...")


def extract_eml(file):
    print("Extracting EML content...")

    data = email_util.getEmailData(file)
    sender = data["from"]
    recipients = data["to"]
    ccs = data["cc"]
    bccs = data["bcc"]
    subject = data["subject"]
    datetimeObj = data["datetime"]
    text = data["text"]

    extracted_triples = []
    timestamp = time.time_ns()

    ID = "email_" + str(timestamp)
    email = individual(ID)
    extractedEntities = []
    extractedEntities.append(ExtractedFileInfo(file=file, concept=email.iri))

    attrs = []
    if recipients is not None:
        for recipient in recipients:
            attrs.append((constants.EVENT_GOL_PLACEHOLDER, rdfString(recipient)))

    if ccs is not None:
        for cc in ccs:
            attrs.append((constants.EVENT_GOL_PLACEHOLDER, rdfString(cc)))

    if bccs is not None:
        for bcc in bccs:
            attrs.append((constants.EVENT_GOL_PLACEHOLDER, rdfString(bcc)))

    if datetimeObj is not None:
        attrs.append((constants.EVENT_TMP_PLACEHOLDER, dateTime(datetimeObj)))

    # create a new email instance
    extracted_triples.append(conceptAssertion(email, constants.EMAIL_CONCEPT))

    # add description (link to file)
    extracted_triples.append((email, Concept(constants.DESCR_IRI), rdfString("<a href='/file/details?file=" + str(file.id) + "'>" + str(file.title) + "</a>")))

    # create an event describing the action "sender sends email to recipients, ccs, bccs at datetime"
    extracted_triples.extend(event(ID + "_send",
                                   rdfString(sender) if sender is not None else None,
                                   constants.SEND_CONCEPT,
                                   email,
                                   attrs))

    # create an individual si of type sender and add an event describing the action "email has si"
    # as well as a triple (si, {descr}, sender)
    if sender is not None:
        senderIndividual = individual(ID + "_sender")
        extractedEntities.append(ExtractedFileInfo(file=file, concept=senderIndividual.iri))
        extracted_triples.append(conceptAssertion(senderIndividual, constants.SENDER_CONCEPT))
        extracted_triples.extend(event(ID + "_sender",
                                       email,
                                       constants.HOLD_CONCEPT,
                                       senderIndividual,
                                       []))
        extracted_triples.append((senderIndividual, Concept(constants.DESCR_IRI), rdfString(sender)))

    # for each recipient create an individual ri of type recipient and add an event describing the action "email has ri"
    # as well as a triple (ri, {descr}, recipient)
    if recipients is not None:
        for i, recipient in enumerate(recipients):
            recipientIndividual = individual(ID + "_recipient" + str(i))
            extractedEntities.append(ExtractedFileInfo(file=file, concept=recipientIndividual.iri))
            extracted_triples.append(conceptAssertion(recipientIndividual, constants.RECIPIENT_CONCEPT))
            extracted_triples.extend(event(ID + "_recipient" + str(i),
                                           email,
                                           constants.HOLD_CONCEPT,
                                           recipientIndividual,
                                           []))
            extracted_triples.append((recipientIndividual, Concept(constants.DESCR_IRI), rdfString(recipient)))

    # create an individual si of type emailSubject and add an event describing the action "email has si"
    # as well as a triple (si, {descr}, subject)
    if subject is not None:
        subjectIndividual = individual(ID + "_subject")
        extractedEntities.append(ExtractedFileInfo(file=file, concept=subjectIndividual.iri))
        extracted_triples.append(conceptAssertion(subjectIndividual, constants.SUBJECT_CONCEPT))
        extracted_triples.extend(event(ID + "_subject",
                                       email,
                                       constants.HOLD_CONCEPT,
                                       subjectIndividual,
                                       []))
        extracted_triples.append((subjectIndividual, Concept(constants.DESCR_IRI), rdfString(subject)))

    # create an individual dti of type emailDatetime and add an event describing the action "email has dti"
    # as well as a triple (dti, {descr}, text)
    if datetimeObj is not None:
        datetimeIndividual = individual(ID + "_emailDatetime")
        extractedEntities.append(ExtractedFileInfo(file=file, concept=datetimeIndividual.iri))
        extracted_triples.append(conceptAssertion(datetimeIndividual, constants.EMAIL_DATETIME_CONCEPT))
        extracted_triples.extend(event(ID + "_datetime",
                                       email,
                                       constants.HOLD_CONCEPT,
                                       datetimeIndividual,
                                       []))
        extracted_triples.append((datetimeIndividual, Concept(constants.DESCR_IRI), dateTime(datetimeObj)))

    # create an individual ti of type text and add an event describing the action "email has ti"
    # as well as a triple (ti, {descr}, text)
    if text is not None:
        textIndividual = individual(ID + "_text")
        extractedEntities.append(ExtractedFileInfo(file=file, concept=textIndividual.iri))
        extracted_triples.append(conceptAssertion(textIndividual, constants.TEXT_CONCEPT))
        extracted_triples.extend(event(ID + "_text",
                                       email,
                                       constants.HOLD_CONCEPT,
                                       textIndividual,
                                       []))
        extracted_triples.append((textIndividual, Concept(constants.DESCR_IRI), rdfString(text)))

        extracted_triples.extend(event(ID + "_talk_about",
                                       email,
                                       constants.TALK_CONCEPT,
                                       rdfString(text),
                                       []))

    print("Extracted " + str(len(extracted_triples)) + " triples")
    spql.insertTriples(extracted_triples)

    for extractedEntity in extractedEntities:
        extractedEntity.save()


def extractUserInfo(user):
    alreadyExtracted = ExtractedUserInfo.objects.filter(user=user).exists()

    if alreadyExtracted:
        return

    print("Extract info of user '" + user.username + "'...")

    extracted_triples = []
    ID = "user_" + str(user.id)
    userIndividual = individual(ID)
    extractedEntities = []
    extractedEntities.append(ExtractedUserInfo(user=user, concept=userIndividual.iri, isUserIndividual=True))

    extracted_triples.append(conceptAssertion(userIndividual, constants.USER_CONCEPT))

    fullName = user.first_name + " " + user.last_name

    extracted_triples.append(
        (userIndividual, Concept(constants.DESCR_IRI), rdfString(user.username + (" (" + str(fullName) + ")" if len(fullName) > 1 else ""))))

    extracted_triples.extend(event(ID + "_create",
                                   None,
                                   constants.CREATE_CONCEPT,
                                   userIndividual,
                                   [(constants.EVENT_TMP_PLACEHOLDER, dateTime(user.date_joined))]))

    usernameIndividual = individual(ID + "_username")
    extractedEntities.append(ExtractedUserInfo(user=user, concept=usernameIndividual.iri))
    extracted_triples.append(conceptAssertion(usernameIndividual, constants.USERNAME_CONCEPT))
    extracted_triples.extend(event(ID + "_username",
                                   userIndividual,
                                   constants.HOLD_CONCEPT,
                                   usernameIndividual,
                                   []))
    extracted_triples.append((usernameIndividual, Concept(constants.DESCR_IRI), rdfString(user.username)))

    firstNameIndividual = individual(ID + "_first_name")
    extractedEntities.append(ExtractedUserInfo(user=user, concept=firstNameIndividual.iri))
    extracted_triples.append(conceptAssertion(firstNameIndividual, constants.FIRST_NAME_CONCEPT))
    extracted_triples.extend(event(ID + "_first_name",
                                   userIndividual,
                                   constants.HOLD_CONCEPT,
                                   firstNameIndividual,
                                   []))
    extracted_triples.append((firstNameIndividual, Concept(constants.DESCR_IRI), rdfString(user.first_name)))

    lastNameIndividual = individual(ID + "_last_name")
    extractedEntities.append(ExtractedUserInfo(user=user, concept=lastNameIndividual.iri))
    extracted_triples.append(conceptAssertion(lastNameIndividual, constants.LAST_NAME_CONCEPT))
    extracted_triples.extend(event(ID + "_last_name",
                                   userIndividual,
                                   constants.HOLD_CONCEPT,
                                   lastNameIndividual,
                                   []))
    extracted_triples.append(
        (lastNameIndividual, Concept(constants.DESCR_IRI), rdfString(user.last_name)))

    emailAddressIndividual = individual(ID + "_email_address")
    extractedEntities.append(ExtractedUserInfo(user=user, concept=emailAddressIndividual.iri))
    extracted_triples.append(conceptAssertion(emailAddressIndividual, constants.EMAIL_ADDRESS_CONCEPT))
    extracted_triples.extend(event(ID + "_email_address",
                                   userIndividual,
                                   constants.HOLD_CONCEPT,
                                   emailAddressIndividual,
                                   []))
    extracted_triples.append(
        (emailAddressIndividual, Concept(constants.DESCR_IRI), rdfString(user.email)))

    print("Extracted " + str(len(extracted_triples)) + " triples")
    spql.insertTriples(extracted_triples)

    for extractedEntity in extractedEntities:
        extractedEntity.save()
