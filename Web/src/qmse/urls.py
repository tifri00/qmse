from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

import stanza

import login.views as login_views

from app import  settings_utils
from app import stanza_helper
from app import prop_bank_helper
from app import ontology_utils

urlpatterns = [
    path('', include('app.urls')),
    path('admin/', admin.site.urls),
    path('login/', login_views.login_page),
    path('logout/', login_views.logout_page),
    path('file/', include('files.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

prop_bank_helper.init()
ontology_utils.init()
stanza.download(settings_utils.getCurLang())
stanza_helper.initNLP()
