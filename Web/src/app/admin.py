from django.contrib import admin

from app.models import ExtractedUserInfo
from app.models import ExtractedFileInfo

# Register your models here.

admin.site.register(ExtractedUserInfo)
admin.site.register(ExtractedFileInfo)